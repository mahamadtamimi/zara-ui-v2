import React, {useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {commonColors, Link, user} from "@nextui-org/react";
import styles from "@/style/mainMenu.module.scss";
import gift from '@/public/newlodinggif.gif'
import Image from "next/image";

export function UserModule() {

    const dispatch = useDispatch()
    // @ts-ignore
    const user = useSelector((state) => state.user);
    // @ts-ignore
    const token = useSelector((state) => state.token);
    // @ts-ignore
    const basket_id = useSelector((state) => state.basket_id)
    // @ts-ignore
    const basket_product = useSelector((state) => state.basket_product);


    useEffect(() => {

        const requestUrl = `${process.env.API_PATH}/api/v1/me`
        const header = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            body: JSON.stringify({basket_id: basket_id})
        }
        fetch(requestUrl, header)
            .then(res => {
                return res.json()
            })
            .then(data =>{

                dispatch({type: 'LOGIN', payload: {user: data.user, token: data.token}});
                dispatch({type: 'UPDATE_BASKET_ID', payload: data.basket?.id });
                dispatch({type: 'UPDATE_BASKET_PRODUCT', payload: data.basket?.products });
            })


    }, [])


    return (
        <>
            <li>
                {(user) ?
                    <Link href={'/dashboard'}>
                        {user.name}
                    </Link>
                    :
                    <Link href={'/login'}> ورود </Link>
                }

            </li>
            <li className={styles.access_bar_mob_hidden}>
                <Link href="">کمک</Link>
            </li>
            <li className={styles.access_bar_mob_hidden}>
                <Link href="/cart">
                    سبد خرید
                    ({basket_product ? basket_product?.length : 0})

                </Link>
            </li>
        </>


    )
}