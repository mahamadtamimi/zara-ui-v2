"use client"

import React from "react";
import styles from '@/style/mainMenu.module.scss'


export default function SearchComponent() {

    return <div>
        <form action="/search">
            <input name={'s'} className={`w-full   ${styles.search_box}`} type="text" placeholder="جستجو"/>
        </form>

    </div>
}