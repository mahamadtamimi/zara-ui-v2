"use client"
import React, {useEffect, useState} from "react";
import {BurgerIcon} from "./BergurIcon";
import {ZaraLogo} from "./ZaraLogo";
import styles from '@/style/mainMenu.module.scss'
import Link from "next/link";
import {CloseIcon} from "./CloseIcon";
import {motion} from "framer-motion";
import {useDispatch} from "react-redux";


export default function MenuList({mainMenu, indexPage , singlePage}: any) {








    const [menuList, setMenuList] = useState(null)


    const [menuActive, setMenuActive] = useState(0)


    const [showMenu, setShowMenu] = useState(false)


    useEffect(() => {
        // @ts-ignore
        let menuData = []
        {
            Object.keys(mainMenu).map((value, i) => {
                if (mainMenu[value].categories.item !== undefined) {
                    menuData.push(mainMenu[value].categories)
                }
            })
        }
        // @ts-ignore
        setMenuList(menuData)

    }, []);

    function openMenu() {
        setShowMenu(!showMenu)
    }



    return <>
        <div className={styles.burger_icon} onClick={openMenu}>
            {
                showMenu ? <CloseIcon/> : <BurgerIcon/>

            }
        </div>
        <div className={`${styles.logo_sec} ${showMenu && styles.logo_sec_active}`}>
            <div className={`${styles.logo}  ${singlePage && styles.single_sec_logo_hidden}`}>
                <Link href="/" className={'logo-it'}>
                    <ZaraLogo color={indexPage ? '#ecd35d' : '#000'}/>
                </Link>
            </div>

            {/*index menu*/}


            {(indexPage) &&
                <ul className={styles.main_menu_index}>
                    {
                        // @ts-ignore
                        menuList && menuList.map((item, index) => (


                        <li className={styles.main_menu_index_item} key={item.styles}>
                                <span className={styles.main_menu_index_item_item}
                                      onClick={(e) => {
                                          setMenuActive(index)
                                          setShowMenu(true)
                                      }}>
                                      {
                                          item.farsi_name
                                      }
                                    </span>
                        </li>


                    ))}
                </ul>
            }


            {/*desctop menu*/}

            {showMenu &&

                <div className={styles.main_menu}>
                    <div className={`${styles.main_menu_head_for_scroll} scl-efext`}>
                        <ul className={`${styles.main_menu_head} scl-efext`}>
                            {
                                // @ts-ignore
                                menuList && menuList.map((item, index) => (


                                <li className={styles.main_menu_item} key={item.styles}>
                                <span className={styles.main_item_in_menu}
                                      onClick={(e) => setMenuActive(index)}>
                                      {
                                          item.farsi_name
                                      }
                                    </span>
                                </li>


                            ))}
                        </ul>
                    </div>

                    {menuList &&
                        <div className={`${styles.main_menu_list_for_scroll} scl-efext`}>
                            <div className={styles.main_menu_list}>
                                <ul className={styles.main_menu_list_ul}>
                                    {
                                        // @ts-ignore
                                        Object.keys(menuList[menuActive].item).map((subValue, i) => (
                                        <li
                                            className={styles.main_menu_list_ul_item}
                                            key={''}>
                                            <Link
                                                href={
                                                    // @ts-ignore
                                                `/${menuList[menuActive].slug}/${menuList[menuActive].item[subValue].slug}`}
                                                className={styles.sub_item}>

                                                {
                                                    // @ts-ignore
                                                    menuList[menuActive].item[subValue].farsi_name}
                                            </Link>
                                        </li>
                                    ))}

                                </ul>

                            </div>
                        </div>
                    }
                </div>
            }


            {/*mobile nav */}

            {showMenu &&
                <motion.div
                    animate={{x: 0, opacity: 1}}
                    exit={{x: 425, opacity: 0}}
                    initial={{x: 425, opacity: 0}}
                    transition={{duration: 0.2}}
                    className={styles.mobile_nav_sec}>
                    <div className={styles.mobile_nav_header}>
                        <span onClick={() => setShowMenu(false)}>
                               <CloseIcon/>
                        </span>

                        <span>
                             سبد خرید
                         </span>
                    </div>
                    <ul className={styles.mobile_main_menu}>
                        {
                            // @ts-ignore
                            menuList && menuList.map((item) => (


                            <li className={styles.mobile_main_menu_item} key={item.styles}>
                                <span className={styles.main_menu_index_item_item}
                                      onClick={(e) => {
                                          setMenuActive(item.item)
                                          setShowMenu(true)
                                      }}>
                                      {
                                          item.farsi_name
                                      }
                                    </span>
                            </li>


                        ))}
                    </ul>
                    {menuList && <div className={`${styles.mob_main_menu_list_for_scroll} scl-efext`}>
                        <div className={styles.mob_main_menu_list}>
                            <ul className={styles.main_menu_list_ul}>

                                {
                                    // @ts-ignore
                                    Object.keys(menuList[menuActive].item).map((subValue, i) => (
                                    <li
                                        className={styles.mob_main_menu_list_ul_item}
                                        key={''}>
                                        <Link
                                            href={
                                                // @ts-ignore
                                            `/${menuList[menuActive].slug}/${menuList[menuActive].item[subValue].slug}`}
                                            className={styles.sub_item}>

                                            {
                                                // @ts-ignore
                                                menuList[menuActive].item[subValue].farsi_name}
                                        </Link>
                                    </li>
                                ))}

                            </ul>

                        </div>
                    </div>

                    }


                    <div className={styles.mobile_nav_footer}>
                        <div className={styles.mobile_nav_search_footer}>

                            <input type="text" placeholder={'جستجو'}/>
                        </div>


                        <div className={styles.mobile_nav_ul_footer}>
                         <span>
                             خانه
                         </span>
                            <span>
                            ورود
                        </span>

                        </div>
                    </div>

                </motion.div>}


        </div>

    </>

}
