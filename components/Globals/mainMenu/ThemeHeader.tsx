import styles from '@/style/mainMenu.module.scss'
import MenuList from "./MenuList";
import SearchComponent from "./SearchComponent";
import User from "./User";
import {Link} from "@nextui-org/react";
import {getMainMenu} from "@/api/getMainMenu";
import Loader from "@/components/Globals/Loader";


// @ts-ignore
export default async function ThemeHeader({indexPage , singlePage}) {

    console.log(singlePage)
    const mainMenu = await getMainMenu()

    return <>
        <div className={styles.main_head}>

            <div className={styles.main_bar}>
                <MenuList mainMenu={mainMenu} indexPage={indexPage} singlePage={singlePage}/>
            </div>


            <div className={styles.side_bar}>
                <div className={`${styles.search_sec} ${singlePage && styles.single_sec_logo_hidden}`}>

                    <SearchComponent/>


                </div>
                <div className={`${styles.access_bar} ${!indexPage && styles.access_bar_black}`}>
                    <ul className={styles.access_bar_ul}>
                        <User/>

                    </ul>
                </div>
            </div>


        </div>
    </>
}