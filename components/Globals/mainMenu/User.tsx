'use client'


import {Provider} from "react-redux";

import {UserModule} from "./UserModule";
import {PersistGate} from "redux-persist/integration/react";
import {persistor, store} from "@/reduxStore/store";

export default function User() {


    return  <Provider store={store}>
        <PersistGate persistor={persistor}>
            <UserModule/>
        </PersistGate>
    </Provider>
}