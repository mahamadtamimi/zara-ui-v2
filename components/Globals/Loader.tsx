import Image from "next/image";
import gift from "@/public/newlodinggif.gif";
import React from "react";
import {useSelector} from "react-redux";
import styles from '@/style/mainMenu.module.scss'

export default function Loader() {


    return <Image className={styles.loader} src={gift} alt={''} width={60}/>


}