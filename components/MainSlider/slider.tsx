'use client'
import React, {useRef, useState} from 'react';
// Import Swiper React components
import {Swiper, SwiperSlide} from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';

import './styles.css';


import image1
    from '@/public/sliders/IMAGE-landscape-default-fill-7ee4597e-f338-4475-ac5d-98e804f26a8a-default_0.jpg'

import Link from "next/link";
import Image from "next/image";


export default function slider(props : any) {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [sliders, setSliders] = useState(props.data['slider'])
    return (
        <>
            <Swiper
                className="mySwiper swiper-h main-swiper"
                direction={'vertical'}
                pagination={{
                    clickable: true,
                }}

            >
                {sliders?.map((item:any) => {

                    return <SwiperSlide key={item.id}>


                        <Image src={`${process.env.IMAGE_DIRECTORY}${item.src}`} alt='' width={2000} height={100}/>

                    </SwiperSlide>
                })}


            </Swiper>
        </>
    );
}
