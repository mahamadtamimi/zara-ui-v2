'use client'
import React, {useEffect, useRef, useState} from 'react';
// Import Swiper React components
import {Swiper, SwiperSlide} from 'swiper/react';
import styles from '@/style/singleProduct.module.scss'
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/navigation';
import 'swiper/css/thumbs';
import 'swiper/css/pagination';


import '@/style/productSlider.css';

// import required modules
import {FreeMode, Mousewheel, Thumbs, Pagination} from 'swiper/modules';

import {Image} from "@nextui-org/react";


export default function ProductSlider(props: any) {


    const [thumbsSwiper, setThumbsSwiper] = useState(null);

    return (<>
            <div className={styles.product_slider}>
                <div>
                    <div className={'product-main-wrraper'}>

                        <Swiper onSwiper={() => setThumbsSwiper}
                                loop={true}
                                spaceBetween={10}
                                slidesPerView={4}
                                freeMode={true}
                                watchSlidesProgress={true}
                                modules={[FreeMode, Thumbs]}
                                className="mySwiper"
                        >
                            {props.images?.map((item: any) => (

                                <SwiperSlide key={`main-${item.id}`}>
                                    <Image radius={'none'} src={`${process.env.API_PATH}/storage/${item.src}`} alt='' width={70}
                                           height={35}/>
                                </SwiperSlide>
                            ))}

                        </Swiper>


                        <Swiper
                            loop={true}
                            direction={'vertical'}
                            spaceBetween={10}
                            mousewheel={true}
                            pagination={{
                                type: 'progressbar',
                            }}
                            thumbs={{swiper: thumbsSwiper}}
                            modules={[Thumbs, Mousewheel, Pagination]}
                            className="mySwiper2"
                        >
                            {props.images?.map((item: any) => (

                                <SwiperSlide key={`tumb-${item.id}`}>
                                    <Image radius={'none'} className={styles.zIndexController} src={`${process.env.API_PATH}/storage/${item.src}`} alt=''
                                           // width={476}
                                           // height={715}
                                    />
                                </SwiperSlide>
                            ))}
                        </Swiper>


                    </div>

                </div>
            </div>


        </>

    );
}
