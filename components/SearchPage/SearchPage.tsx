'use client'

import {useSearchParams} from 'next/navigation'
import React, {useEffect, useState} from "react";
import InnerProductArchive from "@/components/ArchiveProduct/InnerProductArchive";
import styles from "@/style/search.module.scss";
import ProductCartMedium from "@/components/ProductCart/ProductCartMedium";

export default function SearchPage() {
    const searchParams = useSearchParams()
    const search = searchParams.get('s')

    const [loading, setLoading] = useState(true)
    const [product, setProduct] = useState()

    useEffect(() => {
        const requestUrl = `${process.env.API_PATH}/api/v1/search?s=${search}`
        fetch(requestUrl).then(res => res.json()).then(data => {
            console.log(data)
            setProduct(data.products)
        })

    } , [])

    // @ts-ignore
    return <>
        <div className={styles.sear_sec__}>
            <div className={styles.search_bear}>
                <p>
                    جستجو برای :
                </p>
                <p>
                    {search}
                </p>

            </div>
            <div className={styles.inner_product_archive_sec}>

                {// @ts-ignore
                    product && product.map((element: any, index: any) => {


                    return <div className={styles.main_cart_sec___} key={`product-${index}`}>
                        <ProductCartMedium toman={2500}
                                           data={element}/>
                    </div>


                })}

            </div>
        </div>

    </>
}