'use client'
import styles from '@/style/archiveProduct.module.scss'
import mobileStyles from './mobileStyle.module.scss'
import ProductCartMedium from '@/components/ProductCart/ProductCartMedium';
import Link from "next/link";
import React, {useEffect, useState} from "react";

import {useFormState, useFormStatus} from "react-dom";




const initialState = {
    message: null,
}

function SubmitButton() {
    const {pending} = useFormStatus()

    return (
        <button className="filter-submit-btn" aria-disabled={pending}>مشاهده نتایج</button>
    )
}


export default function InnerProductArchive(props: any) {


    const [size, setSize] = useState([])
    const [color, setColor] = useState([])
    const [price, setPrice] = useState([])
    const [showColorFilter, setShowColorFilter] = useState(false)
    const [showSizeFilter, setShowSizeFilter] = useState(false)
    const [showPriceFilter, setShowPriceFilter] = useState(false)
    const [mainData, setMainData] = useState(props.products.products)

    const [minPrice, setMinPrice] = useState(0)
    const [maxPrice, setMaxPrice] = useState(0)
    const [priceRange, setPriceRange] = useState([])
    const [part, setPart] = useState(0)


    useEffect(() => {

        let price_range: any = []

        mainData.map((item: any, index: any) => {

            price_range.push((item.price * props.products.lir_price));
        })


        price_range.sort(function (a: any, b: any) {
            return a - b;
        });

        setPriceRange(price_range)
        setMinPrice(price_range[0])
        setMaxPrice(price_range[price_range.length - 1])
        setPart((price_range[price_range.length - 1] - price_range[0]) / 5)
        // console.log(price_range[])

    }, [size, color]);



    function updateSize(sizeStr: any) {
        // @ts-ignore
        let index: any = size.indexOf(sizeStr);
        if (index > -1) {
            setSize(
                size.filter(item =>
                    item !== sizeStr
                ));

        } else {

            setSize([

                // @ts-ignore
                ...size,
                // @ts-ignore
                sizeStr
            ]);
        }

    }

    function updateColor(sizeStr: bigint) {
        // @ts-ignore
        const index = color.indexOf(sizeStr)
        if (index > -1) {
            setColor(
                color.filter(item =>
                    item !== sizeStr
                ));

        } else {
            setColor([
                // @ts-ignore
                ...color,
                // @ts-ignore
                sizeStr
            ]);
        }

    }


    function filterDelete(filter: string) {
        switch (filter) {
            case 'size':
                setSize([])
                break
            case 'color':
                setColor([])
                break
            case 'price':
                setPrice([])
                break
        }
    }

    function updatePrice(priceStr: bigint) {
        // @ts-ignore
        const index = price.indexOf(priceStr)
        if (index > -1) {
            setPrice(
                price.filter(item =>
                    item !== priceStr
                ));

        } else {
            setPrice([
                // @ts-ignore
                ...price,
                // @ts-ignore
                priceStr
            ]);
        }

    }

    function setShowColorFilterFn() {
        setShowSizeFilter(false);
        setShowPriceFilter(false);
        setShowColorFilter(!showColorFilter);

    }

    function setShowPriceFilterFn() {
        setShowSizeFilter(false);
        setShowColorFilter(false);
        setShowPriceFilter(!showPriceFilter);

    }

    function setShowSizeFilterFn() {
        setShowSizeFilter(!showSizeFilter);
        setShowColorFilter(false);
        setShowPriceFilter(false);

    }

    function showResult() {
        setShowPriceFilter(false)
        setShowSizeFilter(false)
        setShowColorFilter(false)
    }

    function checkFilter(productSize: any, productColorId: any, productPrice: any) {
        let sizeData: any = []
        let colorData: any = []
        let priceData: any = []
        if (size.length === 0) sizeData = ['ok']
        if (color.length === 0) colorData = ['ok']
        if (price.length === 0) priceData = ['ok']

        productSize.map((element: any, index: any) => {
            // @ts-ignore
            if (size.includes(element)) {
                sizeData.push('ok')
            } else {
                sizeData.push('no')
            }
        })
        // @ts-ignore
        if (color.includes(productColorId)) {
            colorData.push('ok')
        }

        let priceLabel
        // productPrice
        if (productPrice < (minPrice + part)) {
            priceLabel = 1
        } else if (productPrice >= (minPrice + part) && productPrice < (minPrice + (part * 2))) {
            priceLabel = 2
        } else if (productPrice >= (minPrice + (part * 2)) && productPrice < (minPrice + (part * 3))) {
            priceLabel = 3
        } else if (productPrice >= (minPrice + (part * 3)) && productPrice < (minPrice + (part * 4))) {
            priceLabel = 4
        } else {
            priceLabel = 5
        }

        // @ts-ignore
        if (price.includes(priceLabel)) {
            priceData.push('ok')
        }

        if (sizeData.includes('ok') && colorData.includes('ok') && priceData.includes('ok')) {
            return true
        } else {
            return false
        }


    }


    const TypeBar = () => {

        return (
            props.products['types'].length > 1 ?
                <div className={styles.inner_product_archive_type}>
                    <Link href={`/${props.curent[0]}/${props.curent[1]}`}
                          className={`${styles.inner_product_archive_filter_items} ${undefined === props.curent[2] && styles.type_active} `}>
                        مشاهده همه
                    </Link>
                    {props.products['types'].map((item: any) => (
                        <Link
                            key={`${item.slug}`} href={`/${props.curent[0]}/${props.curent[1]}/${item.slug}`}
                            className={`${styles.inner_product_archive_filter_items} ${item.slug === props.curent[2] && styles.type_active}`}>
                            {item.farsi_name}
                        </Link>
                    ))}
                </div> : null
        )
    }


    const FilterBar = () => (


        <div className={styles.inner_product_archive_filter}>
            <span className={styles.inner_product_archive_filter_items}
                  onClick={setShowColorFilterFn}>
                رنگ
                {color.length > 0 ? `(${color.length})` : ''}


            </span>
            <span className={styles.inner_product_archive_filter_items}
                  onClick={setShowSizeFilterFn}>
                سایز
                {size.length > 0 ? `(${size.length})` : ''}
            </span>
            <span className={styles.inner_product_archive_filter_items}
                  onClick={setShowPriceFilterFn}>

                قیمت
                {price.length > 0 ? `(${price.length})` : ''}
            </span>

            <DesktopFilter/>


        </div>


    )

    // @ts-ignore
    const DesktopFilter = () => (
        <>
            {showColorFilter && <div className={styles.color_filter_sec}>
                <div className={styles.color_filter_sec_ff}>
                    {props.products['colors'].map((item: any) => (
                        <span key={item.color}
                              className={`${styles.color_filter_sec_ff_item}  ${
                                  // @ts-ignore
                                  color.includes(item.id) && styles.size_filter_active}`}
                              onClick={() => updateColor(item.id)}>
                        <span className={styles.color_filter_sec_ff_item_color}
                              style={{backgroundColor: item.color}}></span>
                        <span className={styles.color_filter_sec_ff_item_name}>{item.name}</span>
                    </span>
                    ))}


                </div>
                <div className={styles.filter_sec_btn}>

                    <button className={styles.delete_filter} onClick={() => filterDelete(`color`)}>حذف</button>
                    <button className={styles.show_result} onClick={showResult}>مشاهده نتایج</button>
                </div>
            </div>}
            {showSizeFilter && <div className={styles.size_filter_sec}>
                <div className={styles.size_filter_sec_ff}>

                    <span className={`${styles.color_item} ${
                        // @ts-ignore
                        size.includes("S") && styles.size_filter_active}`}
                          onClick={() => updateSize('S')}>S</span>
                    <span className={`${styles.color_item} ${
                        // @ts-ignore
                        size.includes("M") && styles.size_filter_active}`}
                          onClick={() => updateSize('M')}>M</span>
                    <span className={`${styles.color_item} ${
                        // @ts-ignore
                        size.includes("L") && styles.size_filter_active} `}
                          onClick={() => updateSize('L')}>L</span>
                    <span className={`${styles.color_item} ${
                        // @ts-ignore
                        size.includes("XL") && styles.size_filter_active}`}
                          onClick={() => updateSize('XL')}>XL</span>
                </div>
                <div className={styles.filter_sec_btn}>

                    <button className={styles.delete_filter} onClick={() => filterDelete(`size`)}>حذف</button>
                    <button className={styles.show_result} onClick={showResult}>مشاهده نتایج</button>
                </div>
            </div>}
            {showPriceFilter && <div className={styles.price_filter_sec}>
                <div className={styles.price_filter_sec_ff}>

                    <span
                        className={`${styles.price_filter_sec_ff_item} ${
                            // @ts-ignore
                            price.includes(1) && styles.size_filter_active}`}
                        onClick={() =>
                            // @ts-ignore
                            updatePrice(1)}>
                        { // @ts-ignore
                            new Intl.NumberFormat().format(minPrice)} تا {new Intl.NumberFormat().format(minPrice + part)} تومان
                    </span>
                    <span
                        className={`${styles.price_filter_sec_ff_item} ${
                            // @ts-ignore
                            price.includes(2) && styles.size_filter_active}`}
                        onClick={() =>
                            // @ts-ignore
                            updatePrice(2)}>
                        { // @ts-ignore
                            new Intl.NumberFormat().format(minPrice + part)} تا {new Intl.NumberFormat().format(minPrice + (part * 2))} تومان

                    </span>
                    <span
                        className={`${styles.price_filter_sec_ff_item}   ${
                            // @ts-ignore
                            price.includes(3) && styles.size_filter_active}`}
                        onClick={() =>
                            // @ts-ignore
                            updatePrice(3)}>
                                     {
                                         // @ts-ignore
                                         new Intl.NumberFormat().format(minPrice + (part * 2))} تا {new Intl.NumberFormat().format(minPrice + (part * 3))} تومان
                    </span>
                    <span
                        className={`${styles.price_filter_sec_ff_item}  ${
                            // @ts-ignore
                            price.includes(4) && styles.size_filter_active}`}
                        onClick={() =>
                            // @ts-ignore
                            updatePrice(4)}>
                                     {
                                         // @ts-ignore
                                         new Intl.NumberFormat().format(minPrice + (part * 3))} تا {new Intl.NumberFormat().format(minPrice + (part * 4))} تومان
                    </span>
                    <span
                        className={`${styles.price_filter_sec_ff_item} ${
                            // @ts-ignore
                            price.includes(5) && styles.size_filter_active}`}
                        onClick={() =>
                            // @ts-ignore
                            updatePrice(5)}>
                                    تا {
                        // @ts-ignore
                        new Intl.NumberFormat().format(maxPrice)} تومان
                    </span>
                </div>
                <div className={styles.filter_sec_btn}>

                    <button className={styles.delete_filter} onClick={() => filterDelete(`price`)}>حذف</button>
                    <button className={styles.show_result} onClick={showResult}>مشاهده نتایج</button>

                </div>


            </div>}
        </>
    )

    return <section className={styles.main_inner_product_archive}>
        <TypeBar/>
        <FilterBar/>

        <div className={styles.inner_product_archive_sec}>

            {mainData.map((element: any, index: any) => {

                if (checkFilter(element.size, element.color.id, (element.price * props.products.lir_price))) {

                    return <div className={styles.main_cart_sec___} key={`product-${index}`}>
                        <ProductCartMedium toman={props.products.lir_price}
                                           data={element}/>
                    </div>

                }
            })}

        </div>
    </section>

}