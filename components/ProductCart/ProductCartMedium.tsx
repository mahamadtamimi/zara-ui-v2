import styles from './style.module.scss'
import Image from "next/image";
import React, {useState} from "react";
import Link from "next/link";
import {PlusIcon} from "./PlusIcon";
import {BookmarkIcon} from "./BookmarkIcon";

export default function ProductCartMedium(props: any) {


    const myImg = <Image src={`${process.env.API_PATH}/storage/${props.data.image}`} width={319}
                         height={400} alt=''/>


    function addToBookMark() {

    }


    return <div className={styles.product_cart_medium}>

        <div className={styles.product_cart_medium_image}>
            <Link href={`/product/${props.data.slug}?v=${props.data.color.id}`}>
                {myImg}
            </Link>
            <span className={styles.product_cart_medium_image_icon}>
                    <PlusIcon/>
                </span>
        </div>
        <div className={styles.product_cart_medium_info}>
            <div className={styles.product_cart_medium_info_title}>

                <Link href={`/product/${props.data.slug}?v=${props.data.color.id}`}>
                    <h2 className={styles.product_cart_medium_info_title_kk}>
                        {props.data.title}
                    </h2>
                </Link>

                <span className={styles.bookmark_side} onClick={addToBookMark}>
                       {/*<BookmarkIcon/>*/}
                </span>
            </div>
            <div className={styles.product_cart_medium_info_price_sec}>
                     <span className={styles.product_cart_medium_info_price}>
                         {(new Intl.NumberFormat().format(props.data.price))} لیر
                    </span>
                <span className={styles.price_sub}>
                    *{(new Intl.NumberFormat().format(props.toman))} لیر / تومن
                    </span>

            </div>
            <div className={styles.text_left}>
                    <span>
                          {new Intl.NumberFormat().format(props.data.price * props.toman)} تومان
                    </span>
            </div>
        </div>
    </div>
}