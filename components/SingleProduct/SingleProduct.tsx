'use client'
import React, {useEffect, useState} from "react";
import {useSearchParams} from "next/navigation";
import styles from '@/style/singleProduct.module.scss'
import archiveStyle from '@/style/archiveProduct.module.scss'
import ProductSlider from "@/components/ProductSlider/ProductSlider";
import {Button, Link, Select, SelectItem} from "@nextui-org/react";
import {motion} from "framer-motion";
import AddBasket from "@/api/BasketCookie";
import {Image} from "@nextui-org/react";
import ProductCartMedium from "@/components/ProductCart/ProductCartMedium";
import {useDispatch, useSelector} from "react-redux";
import sizeImage from '@/public/panGuid.jpg'

import {FreeMode, Pagination} from "swiper/modules";

import {Swiper, SwiperSlide} from 'swiper/react';
import {
    atan2, chain, derivative, e, evaluate, filter, log, pi, pow, round, sqrt
} from 'mathjs'

import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/pagination';

import './style.css';


export default function SingleProduct(props: any) {


    const [sizeSujest, setSizeSujest] = useState()

    const [allSize, setAllSize] = useState(props.data.size)

    const [sizeInfo, setSizeInfo] = useState(props.data.size_info[0])


    const [sizeValue, setSizeValue] = useState(sizeInfo?.size_values)


    const [sizeInfoSelect, setSizeInfoSelect] = useState(0)


    const [relatedProduct, setRelatedProduct] = useState(props.data.related_product)

    const [productData, setProductData] = useState(props.data.product)

    const [thumbsSwiper, setThumbsSwiper] = useState(null);

    const searchParams = useSearchParams()

    // @ts-ignore
    const version = parseInt(searchParams.get('v'))

    let initialsProduct = props.data.product
    const initial: any = []

    productData.variable_product.map((item: any) => {
        initial[`v-${item.color_id}`] = item
    })
    const [varProduct, setVarProduct] = useState(initial)


    const [showMore, setShowMore] = useState(false)
    const [showAlert, setShowAlert] = useState(false)
    const [addBasket, setAddBasket] = useState(false)

    const [showGuideSize, setShowGuideSize] = useState(false)

    const [size, setSize] = useState(null)
    const [productVersion, setProductVersion] = useState(`v-${version}`)

    const [showHeightTab, setShowHeightTab] = useState(false)
    const [showWeightTab, setShowWeightTab] = useState(false)
    const [showStyleTab, setShowStyleTab] = useState(false)


    const [showFindYourSizeTab, setShowFindYourSizeTab] = useState(false)


    const dispatch = useDispatch()
    // @ts-ignore
    const user = useSelector((state) => state.user);
    // @ts-ignore
    const basket_id = useSelector((state) => state.basket_id);

    // @ts-ignore
    const sizeRedux = useSelector((state) => state.size)

    const [userSizeInfo, setUserSizeInfo] = useState({
        height: 0,
        weight: 0,
        style: 'normal'
    })

    async function addToBasket() {

        const requestUrl = `${process.env.API_PATH}/api/v1/add-to-basket`

        const d = {
            'basket_id': basket_id ? basket_id : '',
            'product_id': productData.id,
            'size': size,
            'variable': version,
            'image_src': varProduct[productVersion]?.images[0].src
        }

        const data = fetch(requestUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(d),
        }).then(res => res.json()).then(data => {
                dispatch({type: 'UPDATE_BASKET_ID', payload: data.id})
                dispatch({type: 'UPDATE_BASKET_PRODUCT', payload: data.products})

                setAddBasket(true)

            }
        )

    }

    function chooseSize(e: any) {

        setSize(e.target.getAttribute('data-id'))
    }

    const EmptySizeAlert = () => (
        <div className={styles.size_alert_box_sec}>
            <div className={styles.size_alert_box}>
                <div className={styles.size_alert_box_padding}>
                    <h3>
                        هشدار
                    </h3>
                    <p>
                        شما باید یک اندازه را انتخاب کنید
                    </p>
                </div>
                <button className={styles.size_alert_box_close} onClick={() => setShowAlert(false)}>
                    بستن
                </button>

            </div>
        </div>
    )

    const colAnime = {
        open: {
            height: 400
        },
        close: {
            height: 150
        }

    }

    const ProductMaintenance = () => (
        <div className={styles.product_main_section_info}>
            <div className={styles.product_main_section_info_for_border}>
                <div className={styles.product_main_section_info_tab}>
                    <h2 className={styles.product_main_section_info_tab_title}>
                        ترکیب و مراقبت
                    </h2>

                    <motion.div
                        variants={colAnime} animate={showMore ? 'open' : 'close'}
                        transition={{duration: 0.2}}
                        className={`${styles.product_main_section_info_pragh} `}
                        dangerouslySetInnerHTML={{__html: productData.maintenance}}></motion.div>
                    <span onClick={() => setShowMore(!showMore)}>
                            مشاهده ی بیشتر
                    </span>
                </div>
            </div>
        </div>
    )

    const BasketTab = () => (
        <div className={styles.basket_tab}>
            <div className={styles.basket_tab_sec}>
                <div className={styles.basket_tab_sec_padding}>
                    <span className={styles.basket_tab_sec_close_item}
                          onClick={() => setAddBasket(false)}>&times;</span>
                    <span className={styles.size_padding_in_sec}>
                            سایز {size} به سبد خرید شما افزوده شد .
                        </span>
                    <div className={styles.basket_tab_sec_product_div}>
                        <Image radius={'none'} src={`${process.env.API_PATH}/storage/${varProduct[productVersion]?.images[0].src}`}
                               width={200} height={400} alt=''/>
                        <div className={styles.basket_tab_sec_product_div_info}>
                            <p>
                                {props.data.title}
                            </p>
                            <span>
                                    {varProduct[productVersion].color.name}
                                </span>
                        </div>


                    </div>

                    <Link href={'/cart'} className={styles.basket_tab_sec_product_div_btn}>
                        مشاهده سبد خرید
                    </Link>
                </div>


                <div className={styles.basket_related_products}>


                    <RelatedProduct inBasket={true}/>

                </div>
            </div>


        </div>
    )

    const start = 120;
    const end = 230;
    // @ts-ignore
    const range = [...Array(end - start + 1).keys()].map(x => x + start);


    const weightStart = 25;
    const weightEnd = 180;
    // @ts-ignore
    const weightRange = [...Array(weightEnd - weightStart + 1).keys()].map(x => x + weightStart);


    function submitCalculateSize() {


        dispatch({
            type: 'USER_SIZE_UPDATE', payload: {
                weight: userSizeInfo.weight,
                height: userSizeInfo.height,
                style: userSizeInfo.style
            }
        })


    }


    function submitNewSize() {
        dispatch({
            type: 'USER_SIZE_UPDATE', payload: {
                weight: 0,
                height: 0,
                style: 0
            }
        })
    }


    function updateUserHeight(e: any) {
        setUserSizeInfo({
            ...userSizeInfo,
            height: e.target.getAttribute('data-value')
        })

        setShowHeightTab(false)
    }


    function updateUserStyle(e: any) {
        setUserSizeInfo({
            ...userSizeInfo,
            style: e.target.getAttribute('data-value')
        })

        setShowStyleTab(false)
    }

    function updateUserWeight(e: any) {

        setUserSizeInfo({
            ...userSizeInfo,
            weight: e.target.getAttribute('data-value')
        })

        setShowWeightTab(false)

    }

    const [userSizeCalculator, setUserSizeCalculator] = useState({})

    // @ts-ignore
    function onlyUnique(value, index, array) {
        return array.indexOf(value) === index;
    }

    useEffect(() => {
        if (sizeInfo) {
            if (sizeRedux.weight && sizeRedux.height && sizeRedux.style) {

                var userSize: [] = (sizeInfo.size_type.size_type_formula.map((item: any) => {


                    const calFormulaWeight = item.formula.replace('w', sizeRedux.weight);
                    const calFormula = calFormulaWeight.replace('h', sizeRedux.height);

                    return [] = [item.label, evaluate(calFormula)]



                }))



                const testqw = userSize.filter((nmn) => nmn[0] === 'a')

                // @ts-ignore
                var newSizeVal = []
                // @ts-ignore
                sizeInfo.size_values.map((item) => {
                    newSizeVal.push(item.size_id)

                    // return newSizeVal[`${item.size_id}`] = sizeInfo.filter(sizeInfo.size_id == 1)
                })
                // @ts-ignore
                var unique = newSizeVal.filter(onlyUnique);


                var endArray = unique.map((item) => {
                    return {
                        // @ts-ignore
                        ['size']: sizeInfo.size_values.filter((sz) => sz.size_id == item)
                    }

                })

                const test = {
                    a: 40, b: 50
                }

                const sizeDiffrance = endArray.map((item) => {
                    // @ts-ignore
                    return (item.size.map((nmn, endAry = []) => {
                        const label = nmn.label


                        const testqw = userSize.filter((nmnm) => {
                            return nmnm[0] == label
                        })

                        if (testqw[0][1] <= nmn.value) {
                            return [] = [nmn.size_id, `${parseInt(nmn.value) - parseInt(testqw[0][1])}`]
                        } else {
                            return [] = [nmn.size_id, false]
                        }


                    }))

                })



                switch (sizeRedux.style) {
                    case 'normal' :
                        const enfGt = sizeDiffrance.map((inm) => {
                            // @ts-ignore
                            return inm.filter((ineerInm) =>

                                parseInt(ineerInm[1]) && ineerInm[1] <= 20 && ineerInm[1] > 10
                            )
                        })

                        const wUniq = (enfGt.filter((endArrayItem) => endArrayItem.length > 0))
                        if (wUniq.length > 0) {

                            // @ts-ignore
                            const ids = wUniq[0].map((item) => {

                                    return item[0]

                                }
                            )

                            // setSizeSujest(ids[0].display_name)
                            // @ts-ignore
                            const sizeSj = (varProduct[productVersion].sizes.filter((filterId) => filterId.id == ids[0]))

                            setSizeSujest(sizeSj[0].display_name)
                            break


                        } else {
                            // @ts-ignore
                            setSizeSujest('unAvaisable')
                        }


                }


            }
        }


    }, [sizeRedux, sizeInfo])


    const FindYourSizeTab = () => (
        <div className={styles.basket_tab}>

            <div className={styles.find_your_size_tab_sec}>
                <div className={styles.basket_tab_sec_padding}>
                    <span className={styles.basket_tab_sec_close_item}
                          onClick={() => setShowFindYourSizeTab(false)}>&times;</span>


                    <div>
                        <h3>
                            اندازه توصیه می شود
                        </h3>
                        <p>
                            ما اندازه ایده آل شما را بر اساس اندازه گیری های شما محاسبه می کنیم
                        </p>


                    </div>
                    {
                        (sizeRedux && sizeRedux.weight > 0 && sizeRedux.height > 0) ?
                            <>


                                {sizeSujest == 'unAvaisable' ?
                                    <>
                                        <p>
                                            هیچ توصیه ای در دسترس نیست

                                            متأسفیم، ما نمی توانیم اندازه ای برای این محصول توصیه کنیم.
                                        </p>

                                    </>


                                    :

                                    <>
                                        <div className={styles.sujest_sec_main_sec}>
                                            <Image radius={'none'}
                                                src={`${process.env.API_PATH}/storage/${varProduct[productVersion]?.images[0].src}`}
                                                width={300} height={400} alt=''/>


                                            <p className={styles.sujest_sec_size_display_name}>
                                                {sizeSujest}
                                            </p>
                                            <p className={styles.sujest_sec_size_display_name_alerm}>
                                                ممکن است اندازه شما باشد
                                            </p>
                                            <p className={styles.sujest_sec_size_display_name_alerm_sub}>
                                                برای دریافت توصیه دقیق تر، اطلاعات بیشتری اضافه کنید.
                                            </p>
                                        </div>

                                    </>
                                }
                            </>


                            :
                            <>
                                <div className={styles.height_list_ul_main_sec}>

                                    قد خود را وارد کنید

                                    <div>
                                        <div className={styles.height_list_selected} onClick={() => {
                                            setShowHeightTab(true)
                                            setShowWeightTab(false)
                                            setShowStyleTab(false)
                                        }}>
                                            {userSizeInfo.height} cm
                                        </div>


                                        {showHeightTab && < ul className={styles.height_list_ul}>


                                            {range.map((item) => {
                                                return <li key={`${item}-cm`} data-value={item}
                                                           onClick={(e) => updateUserHeight(e)}>
                                                    {item} cm
                                                </li>

                                            })}
                                        </ul>
                                        }

                                    </div>

                                </div>
                                <div className={styles.height_list_ul_main_sec}>

                                    وزن خود را وارد کنید

                                    <div>
                                        <div className={styles.height_list_selected} onClick={() => {
                                            setShowWeightTab(true)
                                            setShowHeightTab(false)
                                            setShowStyleTab(false)
                                        }}>
                                            {userSizeInfo.weight} kg
                                        </div>


                                        {showWeightTab && <ul className={styles.height_list_ul}>
                                            {weightRange.map((item) => {
                                                return <li key={`${item}-kg`} data-value={item}
                                                           onClick={(e) => {
                                                               updateUserWeight(e)
                                                           }}>
                                                    {item}kg
                                                </li>
                                            })}

                                        </ul>}

                                    </div>

                                </div>
                                <div className={styles.height_list_ul_main_sec}>

                                    دوست داری چطوری لباست رو بپوشی؟

                                    <div>
                                        <div className={styles.height_list_selected} onClick={() => {
                                            setShowStyleTab(true)
                                            setShowWeightTab(false)
                                            setShowHeightTab(false)
                                        }}>
                                            {userSizeInfo.style}
                                        </div>

                                        {showStyleTab &&
                                            <ul className={styles.styles_list_ul}>

                                                <li data-value={'very-fit'}
                                                    onClick={(e) => {
                                                        updateUserStyle(e)
                                                    }}>
                                                    بسیار جذب
                                                </li>
                                                <li data-value={'fit'}
                                                    onClick={(e) => {
                                                        updateUserStyle(e)
                                                    }}>
                                                    جذب
                                                </li>
                                                <li data-value={'normal'}
                                                    onClick={(e) => {
                                                        updateUserStyle(e)
                                                    }}>
                                                    نرمال
                                                </li>
                                                <li data-value={'loose'}
                                                    onClick={(e) => {
                                                        updateUserStyle(e)
                                                    }}>
                                                    نیم بگ
                                                </li>
                                                <li data-value={'vary-loose'}
                                                    onClick={(e) => {
                                                        updateUserStyle(e)
                                                    }}>
                                                    بگ
                                                </li>
                                            </ul>
                                        }


                                    </div>

                                </div>
                            </>

                    }


                </div>
                <div>

                    {
                        (sizeRedux && sizeRedux.weight > 0 && sizeRedux.height > 0) ?

                            <Button radius={'none'} onClick={() => submitNewSize()}
                                    className={styles.btn_set_size_to_user}>
                                محاسبه دوباره
                            </Button>
                            :
                            <Button radius={'none'}
                                    isDisabled={!(userSizeInfo.weight && userSizeInfo.height && userSizeInfo.style)}
                                    onClick={() => submitCalculateSize()}
                                    className={styles.btn_set_size_to_user}>
                                ادامه
                            </Button>

                    }
                </div>


            </div>


        </div>
    )


    const GuidYourSizeTab = () => (
        <div className={styles.guid_tab}>
            <div className={styles.guid_tab_sec}>
                <div className={styles.guid_tab_sec_padding}>
                    <span onClick={() => setShowGuideSize(false)}
                          className={styles.guid_tab_sec_close_item}>&times;</span>

                    <div className={styles.guid_image}>


                        <Image radius={'none'} src={`${process.env.API_PATH}/storage/${sizeInfo.size_type.image}`}
                               width={400} height={400} alt=''/>


                    </div>

                    <div className={styles.size_lables}>

                        <Swiper
                            slidesPerView={'auto'}

                            spaceBetween={0}
                            freeMode={true}
                            pagination={{
                                clickable: true,
                            }}
                            dir="rtl"
                            modules={[FreeMode]}
                            className="swiperSize"
                        >
                            {allSize.map((item: any) => (
                                <SwiperSlide key={`slide-${item.display_name}`}>
                                <span onClick={() => setSizeInfoSelect(item.id)} className={styles.size_cart}>

                                    {item.display_name}
                                </span>
                                </SwiperSlide>

                            ))


                            }


                        </Swiper>


                    </div>
                    <div className={styles.size_tables}>
                        <table>
                            <thead>
                            <tr>
                                <th>منطقه</th>
                                <th>cm</th>
                            </tr>
                            </thead>

                            <tbody>

                            {sizeValue && sizeValue.map(
                                (item: any) => {
                                    return <>
                                        {
                                            item.size_id == sizeInfoSelect &&

                                            <tr>
                                                <td>
                                                    {item.label}
                                                </td>
                                                <td>
                                                    cm {item.value}
                                                </td>
                                            </tr>

                                        }
                                    </>


                                })}


                            </tbody>
                        </table>
                    </div>


                </div>


            </div>


        </div>
    )

    const RelatedProduct = (props: any) => (
        <div className={styles.main_related_product_sec}>

            <p className={styles.you_also}>

                شما همچنین ممکن است دوست داشته باشید</p>
            <div className={archiveStyle.inner_product_archive_sec}>

                {
                    relatedProduct.map((item: any) => {

                        return <div className={props.inBasket ? styles.basket_related_products_cart
                            : archiveStyle.main_cart_sec___} key={`product-${item.slug}-v-${item.color.id}`}>
                            <ProductCartMedium toman={2500}
                                               data={item}/>
                        </div>
                    })
                }

            </div>
        </div>
    )


    const ProductDescription = () => (
        <div className={styles.product_side_bar}>
            <div className={styles.add_to_cart}>
                <div className={styles.add_to_cart_info}>
                    <h1 className={styles.main_title}> {productData.title} </h1>
                    <div>
                        <div className={styles.product_cart_medium_info_price_sec}>
                             <span className={styles.product_cart_medium_info_price}>
                                 {(new Intl.NumberFormat().format(productData.price))} لیر
                            </span>
                            <span className={styles.price_sub}>
                            *{(new Intl.NumberFormat().format(props.lir))} لیر / تومن
                            </span>

                        </div>
                        <div className={`${styles.text_left} font-bold`}>
                            {new Intl.NumberFormat().format(productData.price * props.lir)} تومان
                        </div>
                    </div>
                    <div className={styles.product_description}
                         dangerouslySetInnerHTML={{__html: productData.description}}></div>

                </div>


                <div className={styles.add_to_cart_variable}>


                    <div className={styles.add_to_cart_variable_padding}>
                        <p className={styles.add_to_cart_variable_color_label}>
                            رنگ :
                            {productData.variable_product[productVersion]?.color.display_name}

                        </p>
                        <div>
                            {productVersion !== null &&
                                productData.variable_product.map((vPr: any, index: any) => (


                                    <span key={`color-${productData.variable_product[index].color.color}`}
                                          className={`${styles.color_bar_one} 
                                          
                                           ${productVersion == `v-${productData.variable_product[index].color.id}` && styles.color_active}
                                           `}
                                          style={{background: productData.variable_product[index].color.color}}
                                          onClick={() => setProductVersion(`v-${productData.variable_product[index].color.id}`)}>

                                        {productData.variable_product[index].color.display_name}
                                     </span>
                                ))}

                        </div>
                        <div className={styles.add_to_cart_variable_size_sec}>

                            {varProduct[productVersion].sizes.map((item: any) => (

                                <span onClick={(e) => chooseSize(e)} data-id={item.display_name}
                                      key={item.display_name}
                                      className={`${styles.add_to_cart_variable_size}  ${size == item.display_name && styles.size_active}`}>
                                         <span onClick={(e) => chooseSize(e)} data-id={item.display_name}>
                                             {item.display_name}
                                         </span>

                                    {sizeSujest && sizeSujest == item.display_name &&
                                        <span onClick={(e) => chooseSize(e)} data-id={item.display_name}
                                              className={styles.sizeSujest_p}>سایز پیشنهادی</span>}

                                    </span>
                            ))


                            }


                        </div>
                        <div className={styles.size_guid_sec}>
                            {sizeValue &&
                                <span onClick={() => setShowFindYourSizeTab(true)}>
                                محاسبه سایز شما
                            </span>
                            }
                            {sizeValue &&
                                <span onClick={() => setShowGuideSize(true)}>
                                راهنمای سایز
                            </span>
                            }
                        </div>
                    </div>
                    <Button className={styles.add_to_cart_btn} radius={'none'}
                            onClick={() => size ? addToBasket() : setShowAlert(true)}>
                        افزودن
                    </Button>
                </div>
            </div>

        </div>
    )

    return <>
        {showAlert && <EmptySizeAlert/>}

        <div className={styles.product_main}>

            <ProductMaintenance/>

            <ProductSlider images={varProduct[productVersion]?.images}/>

            <ProductDescription/>
        </div>

        <RelatedProduct/>

        {addBasket && <BasketTab/>}
        {showGuideSize && <GuidYourSizeTab/>}
        {showFindYourSizeTab && <FindYourSizeTab/>}
    </>
}