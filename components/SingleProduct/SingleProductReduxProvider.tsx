'use client'
import {Provider} from "react-redux";
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";
import InnerProductArchive from "@/components/ArchiveProduct/InnerProductArchive";
import SingleProduct from "@/components/SingleProduct/SingleProduct";
import React from "react";

export default function SingleProductReduxProvider(props:any){


    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <SingleProduct
                data={props.data}
                lir={props.lir}
            />
        </PersistGate>
    </Provider>

}
