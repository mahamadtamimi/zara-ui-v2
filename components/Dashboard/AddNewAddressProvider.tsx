'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";


import Dashboard from "@/components/Dashboard/Dashboard";
import AddNewAddress from "@/components/Dashboard/AddNewAddress";

export const AddNewAddressProvider = (props : any) => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <AddNewAddress editAddress={props.editAddress} />
        </PersistGate>
    </Provider>
}