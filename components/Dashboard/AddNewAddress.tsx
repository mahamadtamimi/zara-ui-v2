'use client'
import Link from "next/link";

import styles from '@/style/dashboard.module.scss'


import {redirect} from "next/navigation";
import Image from "next/image";
import loadingImage from "@/public/load2.gif";
import React, {useEffect, useState} from "react";
import {Input} from "@nextui-org/input";
import {Button} from "@nextui-org/react";
import {useDispatch, useSelector} from "react-redux";
import {BackIcon} from "@/components/Dashboard/BackIcon";


export default function AddNewAddress(props: any) {

    const initData: object = {
        name: {
            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'fullName',
            label: 'نام کامل',

        },
        state: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'state',
            label: 'استان',

        },
        city: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'city',
            label: 'شهر',

        },
        postCode: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'postCode',
            label: 'کد پستی',


        },
        address: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'address',
            label: 'ادرس',

        },
        phoneNumber: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'phoneNumber',
            label: 'شماره تماس',


        },
    }
    const dispatch = useDispatch()
    const [formData, setFormData] = useState(initData)
    const [success, setSuccess] = useState(false)

    const [pending, setPending] = useState(false)
    // @ts-ignore
    const user = useSelector((state) => state.user)
    // @ts-ignore
    const token = useSelector((state) => state.token)

    useEffect(() => {
        if (props.editAddress){

            const requestUrl = `${process.env.API_PATH}/api/v1/get-address`
            const option = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    token: token
                },
                body: JSON.stringify({id: props.editAddress})
            }
            fetch(requestUrl, option).then(res => res.json()).then(data => {
                // @ts-ignore
                setFormData({
                    ...formData,
                    name: {
                        // @ts-ignore
                        ...formData.name,
                        value: data.name,
                        validate: true,
                    },
                    state: {
                        // @ts-ignore
                        ...formData.state,
                        value: data.state,
                        validate: true,

                    },
                    city: {
                        // @ts-ignore
                        ...formData.city,
                        value: data.city,
                        validate: true,


                    },
                    postCode: {
                        // @ts-ignore
                        ...formData.postCode,
                        value: data.postcode,
                        validate: true,


                    },
                    address: {
                        // @ts-ignore
                        ...formData.address,
                        value: data.address,
                        validate: true,
                    },
                    phoneNumber: {
                        // @ts-ignore
                        ...formData.phoneNumber,
                        value: data.phone_number,
                        validate: true,
                    },


                })


            })


        }



    }, [])


    function updateFeild(e: any) {
        const tr = e.target.getAttribute('data-item')
        const validateConst = e.target.value.length > 0
        const errorConst = e.target.value.length > 0 ? '' : 'لطفا فیلد مورد نظر را به درستی پر کنید'
        switch (tr) {
            case 'fullName' :
                setFormData({
                        ...formData,
                        name: {
                            // @ts-ignore
                            ...formData.name,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )

                break;
            case 'state' :
                setFormData({
                        ...formData,
                        state: {
                            // @ts-ignore
                            ...formData.state,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )

                break;
            case 'city' :
                setFormData({
                        ...formData,
                        city: {
                            // @ts-ignore
                            ...formData.city,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )
                break;
            case 'address' :
                setFormData({
                        ...formData,
                        address: {
                            // @ts-ignore
                            ...formData.address,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )
                break;
            case 'postCode' :
                setFormData({
                        ...formData,
                        postCode: {
                            // @ts-ignore
                            ...formData.postCode,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )
                break;
            case 'phoneNumber' :

                setFormData({
                        ...formData,
                        phoneNumber: {
                            // @ts-ignore
                            ...formData.phoneNumber,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )
                break;
        }


    }

    function getValidateForm() {
        // @ts-ignore
        return formData['name']['validate'] && formData['state']['validate'] && formData['city']['validate'] && formData['postCode']['validate'] && formData['address']['validate'] && formData['phoneNumber']['validate']
    }


    function editAddress() {


        setPending(true)

        const requestUrl = `${process.env.API_PATH}/api/v1/edit-address`
        const data = {
            'id': props.editAddress,
            // @ts-ignore
            'name': formData.name.value,
            // @ts-ignore
            'state': formData.state.value,
            // @ts-ignore
            'city': formData.city.value,
            // @ts-ignore
            'address': formData.address.value,
            // @ts-ignore
            'postcode': formData.postCode.value,
            // @ts-ignore
            'phone_number': formData.phoneNumber.value,

        }

        fetch(requestUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            body: JSON.stringify(data)

        }).then(res => res.json()).then(data => {


            if (data.success) {
                setPending(false)
                setSuccess(true)
            } else {
                // setShowAlert({...showAlert, show: true, massage: data.error})
            }


        })


    }

    function createAddress() {
        setPending(true)

        const requestUrl = `${process.env.API_PATH}/api/v1/add-address`

        const data = {
            // @ts-ignore
            'name': formData.name.value,
            // @ts-ignore
            'state': formData.state.value,
            // @ts-ignore
            'city': formData.city.value,
            // @ts-ignore
            'address': formData.address.value,
            // @ts-ignore
            'postcode': formData.postCode.value,
            // @ts-ignore
            'phone_number': formData.phoneNumber.value,

        }

        fetch(requestUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            body: JSON.stringify(data)

        }).then(res => res.json()).then(data => {

            console.log(data)

            if (data.success) {
                setPending(false)
                setSuccess(true)
            } else {
                // setShowAlert({...showAlert, show: true, massage: data.error})
            }


        })
    }

    {
        success && redirect('/dashboard/address')
    }


    return <>
        <div className={styles.address_form}>
            <div className={styles.address_box_header}>
                <span className={styles.address_box_header_title}>
                    <span className={styles.address_box_header_title_txt}>
                     <Link href={'/dashboard/address'}> <BackIcon/></Link>
                    افزودن ادرس
                    </span>

                </span>

            </div>


            <div className={styles.address_tab_form}>
                <div className={styles.address_row}>
                    {Object.keys(formData).map((keyName, i) => {


                        return <div key={i} className={styles.login_tab_form_sec_div}>
                            <Input variant={'underlined'}
                                   classNames={{
                                       label: 'input-label',
                                       inputWrapper: 'input-wrapper'
                                   }}
                                   label={// @ts-ignore
                                       formData[keyName]['label']}
                                   data-item={// @ts-ignore
                                       formData[keyName]['name']}
                                   onChange={(e) => updateFeild(e)}
                                   isInvalid={
                                       // @ts-ignore
                                       !formData[keyName]['validate']}
                                   errorMessage={
                                       // @ts-ignore
                                       !formData[keyName]['validate'] && formData[keyName]['error']}
                                   type="text"
                                   name={// @ts-ignore
                                       formData[keyName]['name']}
                                   value={// @ts-ignore
                                       formData[keyName]['value']}
                            />

                        </div>
                    })}

                    <Button radius="none"
                            disabled={!getValidateForm()}
                            type="submit"
                            onClick={props.editAddress ? editAddress : createAddress}
                            className={`${styles.address_tab_form_btn} ${!getValidateForm() && styles.disable}`}
                    >
                        {pending ? <Image src={loadingImage} alt='' height={20} width={20}/> :
                            'ذخیره'}
                    </Button>

                    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                </div>
            </div>


        </div>


    </>
}