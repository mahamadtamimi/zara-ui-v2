'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";


import Dashboard from "@/components/Dashboard/Dashboard";

export const DashboardProvider = () => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <Dashboard />
        </PersistGate>
    </Provider>
}