export const MoreIcon = () => (<svg width="32"
                             height="32"
                             viewBox="0 0 32 32"
                             xmlns="http://www.w3.org/2000/svg"
                             fill="inherit" stroke="inherit">
    <path
        d="M11.2 16A1.6 1.6 0 1 1 8 16a1.6 1.6 0 0 1 3.2 0ZM17.6 16a1.6 1.6 0 1 1-3.2 0 1.6 1.6 0 0 1 3.2 0ZM24 16a1.6 1.6 0 1 1-3.2 0 1.6 1.6 0 0 1 3.2 0Z"></path>
</svg>)