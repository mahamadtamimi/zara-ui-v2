import Link from "next/link";
import React, {useEffect, useState} from "react";

// import checkUser from "../../../api/checkUser";
import {Icon} from "./Icon";
import styles from '@/style/dashboard.module.scss'
import {useDispatch, useSelector} from "react-redux";
import {redirect} from "next/navigation";
import {Button} from "@nextui-org/react";

export default function Dashboard() {

    // @ts-ignore
    const user = useSelector((state) => state.user)

    const [userData, setUserData] = useState(user)

    const [logoutSuccess, setLogoutSuccess] = useState(false)


    const dispatch = useDispatch()

    if (logoutSuccess) {
        return redirect('/login')
    }


    function logout() {
        dispatch({type: 'LOGIN', payload: {user: null, token: null}});
        setUserData(null)
        setLogoutSuccess(true)
    }




    return <main className={styles.dashboard_sec}>

        <div className={styles.dashboard_menu}>

            <Button as={Link} radius={'none'} className={styles.dashboard_span_data}
                    href={'/dashboard'}>پروفایل</Button>
            <Button as={Link} radius={'none'} className={styles.dashboard_span_data} href={'/dashboard/order-list'}>لیست
                خرید ها</Button>
        </div>

        <div className={styles.dashboard_content}>
            <span>{user?.name}</span>
            <div className={styles.base_dashboard_br}>
                <ul>
                    <li>
                        <Link href={'dashboard/address'}>
                            ادرس ها
                            <span className={styles.icon}>
                                   <Icon/>
                            </span>

                        </Link>
                    </li>
                    <li>
                        <Link href={'/dashboard/change-email'}>
                           <span className={styles.two_wrrop_sec}>
                               <span className={styles.two_wrrop_sec_main_title}>
                                     ایمیل
                               </span>


                               <span className={styles.two_wrrop_sec_main_sub_title}>
                                   {user.email}
                               </span>

                           </span>

                            <span className={styles.icon}>
                                   <Icon/>
                            </span>
                        </Link>
                    </li>
                    <li>
                        <Link href={'/dashboard/change-phone-number'}>
                           <span className={styles.two_wrrop_sec}>
                               <span className={styles.two_wrrop_sec_main_title}>
                                     شماره تماس
                               </span>


                               <span className={styles.two_wrrop_sec_main_sub_title}>
                                   {user.phone_number}
                               </span>

                           </span>
                            <span className={styles.icon}>
                                   <Icon/>
                            </span>
                        </Link>
                    </li>
                    <li>
                        <Link href={'/dashboard/change-password'}>
                            تغییر پسورد
                            <span className={styles.icon}>
                                   <Icon/>
                            </span>
                        </Link>
                    </li>

                </ul>
            </div>


            <Link className={styles.dashboard_footer_link} onClick={() => logout()} href={''}>خروج از حساب کاربری</Link>

        </div>
    </main>
}