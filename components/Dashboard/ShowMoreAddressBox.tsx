'use client'
import Link from "next/link";
import {useEffect, useState} from "react";
import styles from '@/style/dashboard.module.scss'
import {useSelector} from "react-redux";
import {MoreIcon} from "@/components/Dashboard/MoreIcon";
import Image from "next/image";
import loadingIcon from '@/public/newlodinggif.gif'

export default function ShowMoreAddressBox(props: any) {
    const initialsData = props.data
    const [showMenu, setShowMenu] = useState(0)


    // @ts-ignore
    const token = useSelector((state) => state.token)


    const [address, setAddress] = useState()
    const [loading, setLoading] = useState(false)

    useEffect(() => {

        const requestUrl = `${process.env.API_PATH}/api/v1/get-user-address`


        const header = {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            // body: JSON.stringify({basket_id: basket_id})
        }

        fetch(requestUrl, header).then(res => res.json()).then(data => {
            setAddress(data)
        })
    }, [])

    function deleteForm(id : any, address : any) {
        setLoading(true)

        const data = {
            'address_id': id
        }


        const options = {
            method: "POST",
            headers:
                {
                    "Content-Type": "application/json",
                    "token": token
                }
            ,
            body: JSON.stringify(data),

        }
        fetch(`${process.env.API_PATH}/api/v1/remove-address`, options).then(res => res.json())
            .then(data => {
                setAddress(data.address)
            })

        setLoading(false)


    }



    return <>
        {loading && <Image src={loadingIcon} alt={''} width={60} className={styles.loading_gift}/>}
        {
            // @ts-ignore
            address && address.map((item: any) => (
            <div key={item.id} className={styles.address_item}>
                <div>
                    <div className={styles.address_item_name}>
                        {item.name}
                        <span onClick={() => setShowMenu(showMenu == 0 ? item.id : 0)} className={styles.edit_menu_sec}>
                                         <MoreIcon />


                            {showMenu == item.id && <div className={`${styles.edit_menu}`}>
                                <ul>
                                    <li>
                                        <Link href={`/dashboard/address/${item.id}/edit`}>
                                            ویرایش
                                        </Link>

                                    </li>
                                    <li>
                                        <span onClick={() => deleteForm(item.id, setAddress)}>
                                            حذف
                                        </span>
                                    </li>
                                </ul>

                            </div>}
                             </span>
                    </div>
                    <span className={styles.address_item_span}>{item.gift ? 'شخصی' : 'هدیه'}</span>
                </div>
            </div>
        ))
        }
    </>


}