export const Icon = () => (

    <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill="inherit" stroke="inherit"
         className="zds-selection-cell__selection-icon--default">
        <path fillRule="evenodd" clipRule="evenodd"
              d="M15.336 12 8.624 4.33l.752-.66L16.665 12l-7.289 8.33-.752-.66L15.336 12Z"></path>
    </svg>


)