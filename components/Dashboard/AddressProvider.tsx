'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";


import Dashboard from "@/components/Dashboard/Dashboard";
import Address from "@/components/Dashboard/Address";

export const AddressProvider = () => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <Address />
        </PersistGate>
    </Provider>
}