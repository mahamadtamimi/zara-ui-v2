'use client'
import styles from '@/style/orderDetail.module.scss'
import moment from 'moment';
import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import Image from "next/image";
import cartStyles from '@/style/cart.module.scss';

export default function OrderDetail(props: any) {

    const [orderDetail, setOrderDetail] = useState()
    // @ts-ignore
    const token = useSelector((state) => state.token)

    useEffect(() => {
        const url = `${process.env.API_PATH}/api/v1/orders/${props.orderId}/order-detail`


        fetch(url, {
            headers: {
                "Content-Type": "application/json",
                token: token
            },
        }).then(res => res.json()).then(date => {

            setOrderDetail(date)
        })

    }, [])


    // @ts-ignore
    return <div className={styles.order_detail_sec}>
        {orderDetail &&
            <div className={styles.order_detail_main_sec}>
                <div className={styles.order_detail_main_sec_head}>
                    <div className={styles.order_detail_main_sec_head_main}>

                        <p>
                            شماره سفارش :
                            #{
                            // @ts-ignore
                            orderDetail.id}
                        </p>


                        <p>
                            {
                                // @ts-ignore
                                moment(orderDetail.updated_at).locale('fa').format('YYYY/MM/DD HH:mm:ss')}
                        </p>


                        <p>
                            وضعیت :
                            {
                                // @ts-ignore
                                orderDetail.status ? <span className={styles.text_grree}>
                                    پرداخت شد
                                </span> :
                                    <span className={styles.text_redd}>
                                    ناموفق
                                </span>
                            }
                        </p>


                    </div>
                    <p>
                        : ادرس
                    </p>

                    <div className={styles.order_adrees_box___}>


                        <p>
                            {// @ts-ignore
                                orderDetail.address.name}
                        </p>
                        <p>
                            {// @ts-ignore
                                orderDetail.address.state}
                        </p>
                        <p>
                            {// @ts-ignore
                                orderDetail.address.city}
                        </p>
                        <p>
                            {// @ts-ignore
                                orderDetail.address.address}
                        </p>
                        <p>
                            {// @ts-ignore
                                orderDetail.address.phone_number}
                        </p>
                    </div>

                </div>
                <p className={styles.cart_title_me__}>
                    سبد
                </p>

                <div className={`${cartStyles.cart_tab_item_sec} ${styles.cart_sec} `}>
                    {// @ts-ignore
                        orderDetail.basket.products.map((item) => {

                            return <div key={`cart-${item.title}-${item.pivot.size}-${item.pivot.quantity}`}
                                        className={cartStyles.cart_tab_item}>
                                <Image src={`${process.env.API_PATH}/storage/${item.pivot.image_src}`} alt=''
                                       width={300}
                                       height={300}/>
                                <div className={cartStyles.cart_tab_item_header}>
                                    <p>{item.title}</p>
                                    {/*<DeleteProductBtn id={item.pivot.id}/>*/}
                                </div>


                                <div className={cartStyles.cart_tab_item_price}>
                                    <div className={cartStyles.product_cart_medium_info_price_sec}>
                                     <span className={cartStyles.product_cart_medium_info_price}>
                                         {(new Intl.NumberFormat().format(item.price * item.pivot.quantity))} لیر
                                     </span>
                                        <span className={styles.price_sub}>


                                    </span>

                                    </div>
                                    <div className={cartStyles.text_left}>
                                        {   // @ts-ignore
                                            new Intl.NumberFormat().format(item.price * item.pivot.quantity)} لیر
                                    </div>
                                </div>

                                <span className={cartStyles.cart_tab_item_attribute}>
                            {item.pivot.size} | {item.pivot.color} | {item.pivot.quantity}
                        </span>


                            </div>


                        })}
                </div>

            </div>

        }

        <br/><br/><br/><br/><br/><br/>
    </div>
}