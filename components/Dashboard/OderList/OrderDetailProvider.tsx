'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";


import OrderDetail from "@/components/Dashboard/OderList/OrderDetail";

export const OrderDetailProvider = (props : any) => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <OrderDetail orderId={props.orderId}/>
        </PersistGate>
    </Provider>
}