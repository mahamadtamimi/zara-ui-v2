import Link from "next/link";
import React, {useEffect, useState} from "react";

import styles from '@/style/dashboard.module.scss'
import {useSelector} from "react-redux";
import {redirect, useSearchParams} from "next/navigation";
import {Button, divider} from "@nextui-org/react";
import {Icon} from "../Icon";
import moment from 'moment';
import alertBox from "@/style/alertbox.module.scss";

export default function OrderList() {

    const searchParams = useSearchParams()

    const success = searchParams.get('success')

    const [successMassage, setSuccessMassage] = useState({
        show: false,
        massage: ''
    })


    // @ts-ignore
    const user = useSelector((state) => state.user)
    // @ts-ignore
    const token = useSelector((state) => state.token)

    const [orderList, setOrderList] = useState([])

    useEffect(() => {
        {
            !user && redirect('/login')
        }


    })

    useEffect(() => {
        const requestUrl = `${process.env.API_PATH}/api/v1/get-order`


        const header = {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            // body: JSON.stringify({basket_id: basket_id})
        }

        fetch(requestUrl, header).then(res => res.json()).then(data => {


            setOrderList(data)
        })


        switch (success) {
            case 'ok' :
                setSuccessMassage({
                    ...successMassage,
                    show: true,
                    massage: 'پرداخت شما با موفقیت انجام شد'
                })

                break

            case 'no' :
                setSuccessMassage({
                    ...successMassage,
                    show: true,
                    massage: 'پرداخت ناموفق بود!'
                })
                break

            default:
                setSuccessMassage({

                    show: false,
                    massage: ''

                })


        }

    }, [])


    const PaymentAlert = () => (
        <div className={alertBox.size_alert_box_sec}>
            <div className={alertBox.size_alert_box}>
                <div className={alertBox.size_alert_box_padding}>
                    <h3>
                        هشدار
                    </h3>
                    <p>{successMassage.massage}</p>

                </div>
                <button className={alertBox.size_alert_box_close} onClick={() => setSuccessMassage({
                    ...successMassage,
                    show: false,
                    massage: ''
                })}>
                    بستن
                </button>

            </div>
        </div>
    )

    return <>
        {successMassage.show && <PaymentAlert/>}
        <main className={styles.dashboard_sec}>

            <div className={styles.dashboard_menu}>

                <Button as={Link} radius={'none'} className={styles.dashboard_span_data}
                        href={'/dashboard'}>پروفایل</Button>
                <Button as={Link} radius={'none'} className={styles.dashboard_span_data} href={'/dashboard/order-list'}>لیست
                    خرید ها</Button>
            </div>

            {
                orderList && <div className={styles.dashboard_content}>
                    {orderList.map((item) => {
                        // moment.locale('fa');
                        // @ts-ignore
                        const date = moment(item.updated_at)
                        const dataFormat = date.locale('fa').format('YYYY/MM/DD HH:mm:ss')

                        return <div className={styles.order_cart__} key={
                            // @ts-ignore
                            item.id}>
                            <Link href={
                                // @ts-ignore
                                `/dashboard/order-list/${item.id}`}>
                                <div className={styles.order_cart__header__}>
                                    <p>
                                        شماره سفارش :
                                        <span className={styles.order_id}>
                                      #{   // @ts-ignore
                                            item.id}
                                </span>

                                    </p>
                                    <p>
                                        {dataFormat}
                                    </p>
                                </div>
                                <div className={styles.order_cart__body__}>
                                    <p>
                                        وضعیت :
                                        {   // @ts-ignore
                                            item.status ? <span className={styles.text_grree}>
                                    پرداخت شد
                                </span> :
                                                <span className={styles.text_redd}>
                                    ناموفق
                                </span>
                                        }
                                    </p>

                                    <p>
                                        {   // @ts-ignore
                                            item.price}


                                        تومان
                                    </p>

                                </div>

                                <span className={styles.show_more}>
                                    مشاهده بیشتر
                                </span>
                            </Link>

                        </div>
                    })}
                </div>

            }
        </main>
    </>


}