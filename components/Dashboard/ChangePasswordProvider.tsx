'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";


import Dashboard from "@/components/Dashboard/Dashboard";
import ChangeEmail from "@/components/Dashboard/ChangeEmail";
import ChangePhoneNumber from "@/components/Dashboard/ChangePhoneNumber";
import ChangePassword from "@/components/Dashboard/ChangePassword";

export const ChangePasswordProvider = () => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <ChangePassword />
        </PersistGate>
    </Provider>
}