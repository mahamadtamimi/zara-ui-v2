'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";


import Dashboard from "@/components/Dashboard/Dashboard";
import ChangeEmail from "@/components/Dashboard/ChangeEmail";

export const ChangeEmailProvider = () => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <ChangeEmail />
        </PersistGate>
    </Provider>
}