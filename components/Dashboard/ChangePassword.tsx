'use client'
import styles from '@/style/dashboard.module.scss'
import Image from "next/image";
import loadingImage from "@/public/load2.gif";
import {Input} from "@nextui-org/input";
import alarmStyles from '@/style/singleProduct.module.scss'
import React, {useEffect, useState} from "react";
import {EyeFilledIcon} from "@/components/Auth/EyeFilledIcon";
import {EyeSlashFilledIcon} from "@/components/Auth/EyeSlashFilledIcon";
import {useDispatch, useSelector} from "react-redux";
import {redirect} from "next/navigation";
import {Button} from "@nextui-org/react";
import Link from "next/link";
import {BackIcon} from "@/components/Dashboard/BackIcon";


export default function ChangePassword() {


    const [success, setSuccess] = useState(false)
    const [isVisible, setIsVisible] = useState(false);
    // @ts-ignore
    const user = useSelector((state) => state.user)
    // @ts-ignore
    const token = useSelector((state) => state.token)
    const [pending, setPending] = useState(false)
    const toggleVisibility = () => setIsVisible(!isVisible);
    const [showAlarm, setShowAlarm] = useState({
        show: false,
        massage: ''
    })
    const [formData, setFormData] = useState({
        password: {
            value: '',
            validate: false,
            massage: 'لطفا بیش از شش کاراکتر را وارد نماید'
        },
        newPassword: {
            value: '',
            validate: false,
            massage: 'لطفا بیش از شش کاراکتر را وارد نماید'
        },
        rePassword: {
            value: '',
            validate: false,
            massage: 'لطفا بیش از شش کاراکتر را وارد نماید'
        }

    })

    const dispatch = useDispatch()

    function checkFormValidate(e: any) {
        const target = e.target.getAttribute('data-role')
        const validate = e.target.value.length > 6
        switch (target) {
            case 'password' :


                setFormData({
                    ...formData,
                    password: {
                        ...formData.password,
                        value: e.target.value,
                        validate: validate,
                        massage: 'لطفا بیش از شش کاراکتر را وارد نماید'
                    }
                })

                break;
            case 'newPassword' :


                setFormData({
                    ...formData,
                    newPassword: {
                        ...formData.newPassword,
                        value: e.target.value,
                        validate: validate,
                        massage: 'لطفا بیش از شش کاراکتر را وارد نماید'
                    }
                })
                break


            case 'rePassword' :

                const rePasswordValidate = e.target.value === formData.newPassword.value && e.target.value.length > 0

                setFormData({
                    ...formData,
                    rePassword: {
                        ...formData.rePassword,
                        value: e.target.value,
                        validate: rePasswordValidate,

                        massage: 'عدم تطابق پسورد ها'
                    }
                })
                break
        }

    }


    function submitHandle() {
        setPending(true)
        const requestUrl = `${process.env.API_PATH}/api/v1/change-password`


        const header = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            body: JSON.stringify({
                'password': formData.password.value,
                'new_password': formData.newPassword.value,

            })
        }

        fetch(requestUrl, header).then(res => {
            if (res.status === 403) {
                setPending(false)
                setShowAlarm({show: true, massage: 'پسورد نادرست !'})
                return
            }


            return res.json()
        }).then(data => {
            if (data?.success) {

                setSuccess(true)
            }

            console.log(data)
        })
        setPending(false)
    }

    {
        success && redirect('/dashboard')
    }

    const PasswordIncorevt = () => (
        <div className={alarmStyles.size_alert_box_sec}>
            <div className={alarmStyles.size_alert_box}>
                <div className={alarmStyles.size_alert_box_padding}>
                    <h3>
                        هشدار
                    </h3>
                    <p>
                        {showAlarm.massage}
                    </p>
                </div>
                <button className={alarmStyles.size_alert_box_close} onClick={
                    // @ts-ignore
                    () => setShowAlarm(false)}>
                    بستن
                </button>

            </div>
        </div>
    )

    return <>
        {showAlarm.show && <PasswordIncorevt/>}
        <div className={styles.dashboard_sec}>


            <div className={styles.change_password_sec}>
                <div className={styles.address_box_header}>
                    <div className={styles.address_box_header}>
                <span className={styles.address_box_header_title}>
                    <span className={styles.address_box_header_title_txt}>
                     <Link href={'/dashboard'}> <BackIcon/></Link>
                          بازگشت
                    </span>

                </span>


                    </div>


                </div>
                <span className={styles.change_password_sec_sub__title}>تغیر پسورد</span>

                <div className={''}>
                    <div className={styles.dashboard_sec_input_wrapper}>

                        <Input
                            label="پسورد فعلی"
                            classNames={{
                                label: 'input-label',
                                inputWrapper: 'input-wrapper'
                            }}
                            onChange={(e) => checkFormValidate(e)}
                            variant={'underlined'}
                            data-role={'password'}
                            value={formData.password.value}
                            errorMessage={!formData.password.validate && formData.password.massage}
                            isInvalid={!formData.password.validate}
                            endContent={
                                <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                    {isVisible ? (
                                        <EyeSlashFilledIcon className="text-xl text-gray-600 pointer-events-none"/>
                                    ) : (
                                        <EyeFilledIcon className="text-xl text-gray-600 pointer-events-none"/>
                                    )}
                                </button>
                            }
                            type={isVisible ? "text" : "password"}

                        />

                    </div>
                    <div className={styles.dashboard_sec_input_wrapper}>

                        <Input
                            label="پسورد جدید"
                            classNames={{
                                label: 'input-label',
                                inputWrapper: 'input-wrapper'
                            }}
                            onChange={(e) => checkFormValidate(e)}
                            variant={'underlined'}
                            data-role={'newPassword'}
                            value={formData.newPassword.value}
                            errorMessage={!formData.newPassword.validate && formData.newPassword.massage}
                            isInvalid={!formData.newPassword.validate}
                            endContent={
                                <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                    {isVisible ? (
                                        <EyeSlashFilledIcon className="text-xl text-gray-600 pointer-events-none"/>
                                    ) : (
                                        <EyeFilledIcon className="text-xl text-gray-600 pointer-events-none"/>
                                    )}
                                </button>
                            }
                            type={isVisible ? "text" : "password"}

                        />

                    </div>


                    <div className={styles.dashboard_sec_input_wrapper}>

                        <Input
                            label="پسورد تکرار"
                            classNames={{
                                label: 'input-label',
                                inputWrapper: 'input-wrapper'
                            }}
                            onChange={(e) => checkFormValidate(e)}
                            variant={'underlined'}
                            data-role={'rePassword'}
                            value={formData.rePassword.value}
                            errorMessage={!formData.rePassword.validate && formData.rePassword.massage}
                            isInvalid={!formData.rePassword.validate}
                            endContent={
                                <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                    {isVisible ? (
                                        <EyeSlashFilledIcon className="text-xl text-gray-600 pointer-events-none"/>
                                    ) : (
                                        <EyeFilledIcon className="text-xl text-gray-600 pointer-events-none"/>
                                    )}
                                </button>
                            }
                            type={isVisible ? "text" : "password"}

                        />

                    </div>
                    <Button radius={'none'} onClick={submitHandle}
                            disabled={!(formData.password.validate && formData.rePassword.validate && formData.newPassword.validate)}
                            type="submit"
                            className={`${styles.main_btn} ${!(formData.password.validate && formData.rePassword.validate && formData.newPassword.validate) && styles.disable}`}>
                        {pending ? <Image className={styles.loader} src={loadingImage} alt='' height={20} width={20}/> :
                            'افزودن'

                        }
                    </Button>

                </div>


            </div>


        </div>

    </>


}