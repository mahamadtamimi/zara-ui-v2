import Link from "next/link";
import styles from '@/style/dashboard.module.scss'
import {Button} from "@nextui-org/react";
import ShowMoreAddressBox from "@/components/Dashboard/ShowMoreAddressBox";
import {BackIcon} from "@/components/Dashboard/BackIcon";

// import getAddress from "../../../api/getAddress";

// import ShowMoreAddressBox from "../ShowMoreAddressBox/ShowMoreAddressBox";

export default function Address() {





    return <>
        <div className={styles.dashboard_sec}>

            <div className={styles.address_box_header}>
                <span className={styles.address_box_header_title}>
                    <span  className={styles.address_box_header_title_txt}>
                     <Link href={'/dashboard'} > <BackIcon /></Link>
                            ادرس ها
                    </span>

                </span>


                <Button radius={'none'} as={Link} className={styles.add_address_btn} href={'/dashboard/address/new'}>افزودن
                    ادرس</Button>
            </div>

            <div className={styles.address_item_sec}>
                <ShowMoreAddressBox />
            </div>


        </div>


    </>
}