'use client'
import styles from '@/style/dashboard.module.scss'
import Image from "next/image";
import loadingImage from "@/public/load2.gif";
import {Input} from "@nextui-org/input";
import alarmStyles from '@/style/singleProduct.module.scss'
import React, {useEffect, useState} from "react";
import {EyeFilledIcon} from "@/components/Auth/EyeFilledIcon";
import {EyeSlashFilledIcon} from "@/components/Auth/EyeSlashFilledIcon";
import {useDispatch, useSelector} from "react-redux";
import {redirect} from "next/navigation";
import Link from "next/link";
import {BackIcon} from "@/components/Dashboard/BackIcon";


export default function ChangePhoneNumber() {


    const [success, setSuccess] = useState(false)
    const [isVisible, setIsVisible] = useState(false);
    // @ts-ignore
    const user = useSelector((state) => state.user)
    // @ts-ignore
    const token = useSelector((state) => state.token)
    const [pending, setPending] = useState(false)
    const toggleVisibility = () => setIsVisible(!isVisible);
    const [showAlarm, setShowAlarm] = useState({
        show: false,
        massage: ''
    })
    const [formData, setFormData] = useState({
        password: {
            value: '',
            validate: false,
            massage: 'لطفا بیش از شش کاراکتر را وارد نماید'
        },
        phoneNumber: {
            value: user.phone_number,
            validate: true,
            massage: ''
        }

    })

    const dispatch = useDispatch()

    function checkFormValidate(e: any) {
        const target = e.target.getAttribute('data-role')
        switch (target) {
            case 'password' :
                const validate = e.target.value.length > 6

                setFormData({
                    ...formData,
                    password: {
                        ...formData.password,
                        value: e.target.value,
                        validate: validate,
                        massage: 'لطفا بیش از شش کاراکتر را وارد نماید'
                    }
                })

                break;
            case 'phoneNumber' :
                const phoneNum = convert(e.target.value)

                const phoneValidate = phoneNum.match(
                    /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/
                )


                setFormData({
                    ...formData,
                    phoneNumber: {
                        ...formData.phoneNumber,
                        value: phoneNum,
                        validate: phoneValidate,
                        massage: 'لطفا یک شماره تماس معتبر وارد نمایید'
                    }
                })
                break
        }

    }

    function convert (string: any) {
        return string.replace(/[\u0660-\u0669\u06f0-\u06f9]/g, function (c:any) {
            return c.charCodeAt(0) & 0xf;
        });
    }
    function submitHandle() {
        setPending(true)
        const requestUrl = `${process.env.API_PATH}/api/v1/change-phone-number`


        const header = {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            body: JSON.stringify({phone_number: formData.phoneNumber.value, password: formData.password.value})
        }

        fetch(requestUrl, header).then(res => {
            if (res.status === 403) {
                setPending(false)
                setShowAlarm({show: true, massage: 'پسورد نادرست !'})
                return
            }
            if (res.status === 402) {
                setPending(false)
                setShowAlarm({show: true, massage: 'با این شماره همراه قبلا ثبت نام شده !'})
                return
            }


            return res.json()
        }).then(data => {
            if (data?.success) {
                dispatch({type: 'LOGIN', payload: {user: data.user, token: data.token}});
                setSuccess(true)
            }

            console.log(data)
        })
        setPending(false)
    }

    {
        success && redirect('/dashboard')
    }

    const PasswordIncorevt = () => (
        <div className={alarmStyles.size_alert_box_sec}>
            <div className={alarmStyles.size_alert_box}>
                <div className={alarmStyles.size_alert_box_padding}>
                    <h3>
                        هشدار
                    </h3>
                    <p>
                        {showAlarm.massage}
                    </p>
                </div>
                <button className={alarmStyles.size_alert_box_close} onClick={
                    // @ts-ignore
                    () => setShowAlarm(false)}>
                    بستن
                </button>

            </div>
        </div>
    )

    return <>
        {showAlarm.show && <PasswordIncorevt/>}
        <div className={styles.dashboard_sec}>


            <div className={styles.change_password_sec}>
                <div className={styles.address_box_header}>
                    <div className={styles.address_box_header}>
                <span className={styles.address_box_header_title}>
                    <span className={styles.address_box_header_title_txt}>
                     <Link href={'/dashboard'}> <BackIcon/></Link>
                           بارگشت
                    </span>

                </span>


                    </div>


                </div>
                <span className={styles.change_password_sec_sub__title}>شماره تماس فعلی شما</span>

                <div className={''}>
                    <div className={styles.dashboard_sec_input_wrapper}>

                        <Input
                            label="پسورد فعلی"
                            classNames={{
                                label: 'input-label',
                                inputWrapper: 'input-wrapper'
                            }}
                            onChange={(e) => checkFormValidate(e)}
                            variant={'underlined'}
                            data-role={'password'}
                            value={formData.password.value}
                            errorMessage={!formData.password.validate && formData.password.massage}
                            isInvalid={!formData.password.validate}
                            endContent={
                                <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                    {isVisible ? (
                                        <EyeSlashFilledIcon className="text-xl text-gray-600 pointer-events-none"/>
                                    ) : (
                                        <EyeFilledIcon className="text-xl text-gray-600 pointer-events-none"/>
                                    )}
                                </button>
                            }
                            type={isVisible ? "text" : "password"}

                        />

                    </div>
                    <div className={styles.dashboard_sec_input_wrapper}>
                        <Input type="text"
                               data-role={'phoneNumber'}
                               classNames={{
                                   label: 'input-label',
                                   inputWrapper: 'input-wrapper'

                               }} variant={'underlined'}
                               onChange={(e) => checkFormValidate(e)}
                               value={formData.phoneNumber.value}
                               isInvalid={!formData.phoneNumber.validate}
                               errorMessage={!formData.phoneNumber.validate && formData.phoneNumber.massage}
                               label="شماره تماس جدید"/>

                    </div>
                    <button onClick={submitHandle}
                            disabled={!(formData.password.validate && formData.phoneNumber.validate)}
                            type="submit"
                            className={`${styles.main_btn} ${!(formData.password.validate && formData.phoneNumber.validate) && styles.disable}`}>
                        {pending ? <Image className={styles.loader} src={loadingImage} alt='' height={20} width={20}/> :
                            'افزودن'

                        }
                    </button>

                </div>


            </div>


        </div>

    </>


}