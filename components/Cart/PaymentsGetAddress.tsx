'use client'

import styles from '@/style/adress.module.scss'


import {redirect} from "next/navigation";
import Image from "next/image";
import loadingImage from "@/public/load2.gif";
import React, {useEffect, useState} from "react";

import {useFormState, useFormStatus} from "react-dom";
import {paymentForm} from "./action";
import {Input} from "@nextui-org/input";
import {Button} from "@nextui-org/react";
import {useDispatch, useSelector} from "react-redux";
import {set} from "immutable";
// import getTotalCart from "../../../api/getTotalCart";


const initialState = {
    message: null,
}


export default function PaymentsGetAddress(props: any) {
    // @ts-ignore
    const addressExapmle = []
    const [redirectRoute, setRedirectRoute] = useState(null)

    // @ts-ignore
    const [addressData, setAddressData] = useState(addressExapmle)
    const [formMode, setFormMode] = useState(addressData.length > 0 ? 0 : -1)
    const initData: object = {
        name: {
            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'fullName',
            label: 'نام کامل',

        },
        state: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'state',
            label: 'استان',

        },
        city: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'city',
            label: 'شهر',

        },
        postCode: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'postCode',
            label: 'کد پستی',


        },
        address: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'address',
            label: 'ادرس',

        },
        phoneNumber: {

            value: '',
            validate: false,
            error: ' فیلد مورد نظر را پر کنید .',

            name: 'phoneNumber',
            label: 'شماره تماس',


        },
    }
    const [formData, setFormData] = useState(initData)

    const [pending, setPending] = useState(false)


    const dispatch = useDispatch();

    // @ts-ignore
    const user = useSelector((state) => state.user);
    // @ts-ignore
    const token = useSelector((state) => state.token);
    // @ts-ignore
    const basket_id = useSelector((state) => state.basket_id);

    if (!user || !token)  redirect(`/login?back=payment`)

    useEffect(() => {
        const requestUrl = `${process.env.API_PATH}/api/v1/get-user-address`

        const head = {

            headers: {
                "Content-Type": "application/json",
                "token": token
            },

        }
        fetch(requestUrl, head).then(res => res.json()).then(data => setAddressData(data))


    }, [])

    useEffect(() => {
        function createEditAddressState() {

            return {
                name: {
                    // @ts-ignore
                    ...formData.name,
                    value: addressData[formMode].name,
                    validate: true,


                },
                state: {
                    // @ts-ignore
                    ...formData.state,
                    value: addressData[formMode].state,
                    validate: true,


                },
                city: {
                    // @ts-ignore
                    ...formData.city,
                    value: addressData[formMode].city,
                    validate: true,


                },
                postCode: {
                    // @ts-ignore
                    ...formData.postCode,
                    value: addressData[formMode].postcode,
                    validate: true,


                },
                address: {
                    // @ts-ignore
                    ...formData.address,
                    value: addressData[formMode].address,
                    validate: true,


                },
                phoneNumber: {
                    // @ts-ignore
                    ...formData.phoneNumber,
                    value: addressData[formMode].phone_number,
                    validate: true,


                },
            }


        }


        const data = formMode >= 0 && addressData.length > 0 ? createEditAddressState() : initData


        setFormData(data)
    }, [formMode]);




    function updateFeild(e: any) {
        const tr = e.target.getAttribute('data-item')
        const validateConst = e.target.value.length > 0
        const errorConst = e.target.value.length > 0 ? '' : 'لطفا فیلد مورد نظر را به درستی پر کنید'
        switch (tr) {
            case 'fullName' :
                setFormData({
                        ...formData,
                        name: {
                            // @ts-ignore
                            ...formData.name,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )

                break;
            case 'state' :
                setFormData({
                        ...formData,
                        state: {
                            // @ts-ignore
                            ...formData.state,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )

                break;
            case 'city' :
                setFormData({
                        ...formData,
                        city: {
                            // @ts-ignore
                            ...formData.city,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )
                break;
            case 'address' :
                setFormData({
                        ...formData,
                        address: {
                            // @ts-ignore
                            ...formData.address,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )
                break;
            case 'postCode' :
                setFormData({
                        ...formData,
                        postCode: {
                            // @ts-ignore
                            ...formData.postCode,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )
                break;
            case 'phoneNumber' :

                setFormData({
                        ...formData,
                        phoneNumber: {
                            // @ts-ignore
                            ...formData.phoneNumber,
                            value: e.target.value,
                            validate: validateConst,
                            error: errorConst
                        }
                    }
                )
                break;
        }


    }

    function getValidateForm() {
        // @ts-ignore
        return formData['name']['validate'] && formData['state']['validate'] && formData['city']['validate'] && formData['postCode']['validate'] && formData['address']['validate'] && formData['phoneNumber']['validate']
    }

    function handleForm() {


        const fo = {
            // @ts-ignore
            'name': formData.name.value,
            // @ts-ignore
            'state': formData.state.value,
            // @ts-ignore
            'city': formData.city.value,
            // @ts-ignore
            'address': formData.address.value,
            // @ts-ignore
            'postcode': formData.postCode.value,
            // @ts-ignore
            'phone_number': formData.phoneNumber.value,
            'form_mode': formMode >= 0 ? addressData[formMode].id : formMode,
            'basket_id': basket_id

        };


        const head = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "token": token
            },
            body: JSON.stringify(fo)
        }

        fetch(`${process.env.API_PATH}/api/v1/get-payment-link`, head).then(res => {
                console.log(res.status)
                if (res.status === 401) {
                    dispatch({type: 'LOGIN', payload: {user: null, token: null}});
                    // @ts-ignore
                    setRedirectRoute('/login')
                }
                return res.json()
            }
        ).then(data => {

            setRedirectRoute(data.StartPay)
        })


        console.log('start')
    }

    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
            {
                redirectRoute && redirect(redirectRoute)
            }
        },
        [redirectRoute])

    return <>
        <div className={styles.address_form}>
            <div className={styles.add_address_pay_sec}>
                <h2 className={styles.add_address_pay_title}>آدرس صورتحساب خود را ویرایش کنید</h2>
                <p className={styles.add_address_pay_prag}>
                    برای ثبت سفارش ابتدا باید مشخصات حساب خود را وارد کنید. هر زمان که بخواهید می توانید آنها را در حساب
                    خود تغییر دهید.
                </p>

                <div className={styles.add_address_label_sec}>
                    {addressData?.map((item: any, index: any) => (
                        <div key={item.id} className={styles.address_box__}>
                            <label htmlFor="">
                                <input onChange={() => setFormMode(index)} checked={formMode === index}
                                       type="radio"/>
                                {item.name}
                            </label>
                        </div>
                    ))}
                    <div className={styles.address_box__}>
                        <label htmlFor="">
                            <input onChange={() => setFormMode(-1)} checked={formMode === -1} type="radio"/>
                            جدید
                        </label>
                    </div>
                    <div className={styles.address_box__}>
                        <label htmlFor="">
                            <input onChange={() => setFormMode(-2)} checked={formMode === -2} type="radio"/>
                            ارسال به عنوان هدیه
                        </label>
                    </div>


                </div>


                <div className={styles.address_tab_form}>
                    <div className={styles.address_row}>
                        {Object.keys(formData).map((keyName, i) => {


                            return <div key={i} className={styles.login_tab_form_sec_div}>
                                <Input variant={'underlined'}
                                       classNames={{
                                           label: 'input-label',
                                           inputWrapper: 'input-wrapper'
                                       }}
                                       label={// @ts-ignore
                                           formData[keyName]['label']}
                                       data-item={// @ts-ignore
                                           formData[keyName]['name']}
                                       onChange={(e) => updateFeild(e)}
                                       isInvalid={
                                           // @ts-ignore
                                           !formData[keyName]['validate']}
                                       errorMessage={
                                           // @ts-ignore
                                           !formData[keyName]['validate'] && formData[keyName]['error']}
                                       type="text"
                                       name={// @ts-ignore
                                           formData[keyName]['name']}
                                       value={// @ts-ignore
                                           formData[keyName]['value']}
                                />

                            </div>
                        })}

                    </div>
                </div>

                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            </div>
        </div>
        <div className={styles.continue_bar}>


            <div className={styles.continue_form}>

                <input type="hidden" name={'formMode'}
                       value={addressData[formMode] ? addressData[formMode]['id'] : formMode}/>

                <Button onClick={handleForm} radius={'none'} disabled={!getValidateForm()}
                        className={`${styles.continue_btn} ${!getValidateForm() && styles.disable}`}>
                    {pending ? <Image src={loadingImage} alt='' height={20} width={20}/> :
                        'ادامه'}
                </Button>

            </div>
        </div>

    </>
}