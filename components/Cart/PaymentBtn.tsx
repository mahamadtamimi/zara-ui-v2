// 'use client'
import React from "react";
import Link from "next/link";
import styles from '@/style/cart.module.scss'
import {useSelector} from "react-redux";

export default function PaymentBtn() {
    // @ts-ignore
    const user = useSelector((state) => state.user)
    // @ts-ignore
    const token = useSelector((state) => state.token)




    return <Link

        href={'/cart/get-address'}

        className={styles.payment_btn}>
        پرداخت

    </Link>
}