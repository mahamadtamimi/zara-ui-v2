'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";

import PaymentsGetAddress from "@/components/Cart/PaymentsGetAddress";

export const PaymentsGetAddressProviders = () => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <PaymentsGetAddress />
        </PersistGate>
    </Provider>
}