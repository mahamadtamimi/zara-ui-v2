import React from "react";

import PaymentBtn from "./PaymentBtn";
import styles from '@/style/cart.module.scss'
export default  function Payment(props : any) {

    return <div className={styles.payment_bar}>
        <div className={styles.total_pay}>
            <p className={styles.payment_bar_label}>
                مجموع
            </p>
            <span>
                 {(new Intl.NumberFormat().format(props.total))}
                تومان
            </span>
            <p>
                با احتساب مالیات بر ارزش افزوده
            </p>
        </div>
        <PaymentBtn/>

    </div>
}