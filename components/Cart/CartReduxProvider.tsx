'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";
import {Provider} from "react-redux";
import Cart from "@/components/Cart/Cart";

export function CartReduxProvider(){
    return  <Provider store={store}>
        <PersistGate persistor={persistor}>
            <Cart/>
        </PersistGate>
    </Provider>
}