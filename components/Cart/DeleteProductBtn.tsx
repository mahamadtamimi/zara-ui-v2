'use client'
import React, {useEffect} from "react";
import AddBasket from "@/api/BasketCookie";
import RemoveBasket from "@/api/RemoveBasket";

export default function DeleteProductBtn(props: any) {

    async function deleteProduct(id: bigint) {
        const response = await RemoveBasket({
            'id': id,
        })

    }



    return <>
        <span className={'basket-tab-sec-close-item'} onClick={() => deleteProduct(props.id)}>&times;</span>

    </>
}