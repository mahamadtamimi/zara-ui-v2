'use client'
import React, {useEffect, useState} from "react";
import {Image, Skeleton} from "@nextui-org/react";
import styles from '@/style/cart.module.scss';
import Payment from "./Payment";

import {useDispatch, useSelector} from "react-redux";
import {Card} from "@nextui-org/card";


export default function Cart() {
    const [cart, setCart] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    const [total, setTotal] = useState(true)
    const [lir, setLir] = useState(true)


    const dispatch = useDispatch()
    // @ts-ignore
    const user = useSelector((state) => state.user);
    // @ts-ignore
    const basket_id = useSelector((state) => state.basket_id);
    // @ts-ignore
    const basket_product = useSelector((state) => state.basket_product);


    async function deleteProduct(id: bigint) {

        setIsLoading(true)

        const requestUrl = `${process.env.API_PATH}/api/v1/remove-from-basket`


        const data = {
            'basket_id': basket_id,
            'id': id,
        }
        const head = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        }
        fetch(requestUrl, head).then(res => res.json()).then(data => {
                setIsLoading(false)
                setCart(data.basket.products)
                setTotal(data.total)
                setLir(data.lir_price)
            }
        )
    }

    async function dicQun(id: bigint) {

        setIsLoading(true)
        const requestUrl = `${process.env.API_PATH}/api/v1/dic-from-basket`


        const data = {
            'basket_id': basket_id,
            'id': id,
        }
        const head = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        }
        fetch(requestUrl, head).then(res => res.json()).then(data => {
                setIsLoading(false)
                setCart(data.basket.products)
                setTotal(data.total)
                setLir(data.lir_price)
            }
        )
    }

    async function incQun(id: bigint) {
        setIsLoading(true)

        const requestUrl = `${process.env.API_PATH}/api/v1/inc-from-basket`


        const data = {
            'basket_id': basket_id,
            'id': id,
        }
        const head = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
        }
        fetch(requestUrl, head).then(res => res.json()).then(data => {
                setIsLoading(false)
                setCart(data.basket.products)
                setTotal(data.total)
                setLir(data.lir_price)
            }
        )


    }


    useEffect(() => {
        const requestUrl = `${process.env.API_PATH}/api/v1/get-total-cart`

        fetch(requestUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({'basket_id': basket_id}),
        })
            .then((res) => res.json())
            .then((data) => {

                setIsLoading(false)
                setCart(data.basket.products)
                setTotal(data.total)
                setLir(data.lir_price)
            })


    }, [])


    return <>
        {isLoading &&
            <>
                <div className={styles.cart_main_sec}>
                <span className={styles.cart_tab_head}>
                    سبد خرید
                    ({
                    // @ts-ignore

                })
                </span>
                    {/*<span className={styles.cart_tab_head}>*/}
                    {/*      محصولات محبوب من*/}
                    {/*    <BookmarkIcon/>*/}
                    {/*</span>*/}
                    <div className={styles.cart_tab_item_sec}>

                        <Card     className={styles.cart_tab_item} radius={'none'}>
                            <Skeleton >
                                <div className={`bg-default-300 ${styles.selcton_height}`}></div>
                            </Skeleton>
                            <div className="space-y-3">
                                <Skeleton className="w-3/5 rounded-lg mt-2">
                                    <div className="h-3 w-3/5 rounded-lg bg-default-200"></div>
                                </Skeleton>
                                <Skeleton className="w-4/5 rounded-lg mt-2">
                                    <div className="h-3 w-4/5 rounded-lg bg-default-200"></div>
                                </Skeleton>
                                <Skeleton className="w-2/5 rounded-lg mt-2 pb-3">
                                    <div className="h-3 w-2/5 rounded-lg bg-default-300"></div>
                                </Skeleton>
                                <br/>
                            </div>
                        </Card>


                    </div>
                    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                </div>
                <Payment total={total}/>


            </>

        }


        {
            !isLoading && <>
                <div className={styles.cart_main_sec}>
                <span className={styles.cart_tab_head}>
                    سبد خرید
                    ({
                    // @ts-ignore
                    cart.length
                })
                </span>
                    {/*<span className={styles.cart_tab_head}>*/}
                    {/*      محصولات محبوب من*/}
                    {/*    <BookmarkIcon/>*/}
                    {/*</span>*/}
                    <div className={styles.cart_tab_item_sec}>


                        {   // @ts-ignore
                            cart.map((item: any) => (

                                <div key={`cart-${item.title}-${item.pivot.size}-${item.pivot.quantity}`}
                                     className={styles.cart_tab_item}>
                                    <Image radius={'none'} className={styles.zIndexController}
                                           src={`${process.env.API_PATH}/storage/${item.pivot.image_src}`}
                                           alt=''
                                        // width={400}
                                        // height={300}
                                    />
                                    <div className={styles.cart_tab_item_header}>
                                        <p>{item.title}</p>
                                        {/*<DeleteProductBtn id={item.pivot.id}/>*/}
                                        <span className={'basket-tab-sec-close-item'}
                                              onClick={() => deleteProduct(item.pivot.id)}>&times;</span>
                                    </div>


                                    <div className={styles.cart_tab_item_price}>
                                        <div className={styles.product_cart_medium_info_price_sec}>
                                     <span className={styles.product_cart_medium_info_price}>
                                         {(new Intl.NumberFormat().format(item.price * item.pivot.quantity))} لیر
                                     </span>
                                            <span className={styles.price_sub}>
                                    *{   // @ts-ignore
                                                (new Intl.NumberFormat().format(lir))} لیر / تومن
                                    </span>

                                        </div>
                                        <div className={styles.text_left}>
                                            {   // @ts-ignore
                                                new Intl.NumberFormat().format(item.price * item.pivot.quantity * lir)} تومان
                                        </div>
                                    </div>

                                    <span className={styles.cart_tab_item_attribute}>
                            {item.pivot.size} | {item.pivot.color}
                        </span>


                                    <div className={styles.cart_tab_item_quantity_sec}>
                                        <div className={styles.cart_tab_item_counter_box}>
                                    <span onClick={() => dicQun(item.pivot.id)}
                                          className={styles.cart_tab_item_counter_increment}>-</span>
                                            <span
                                                className={styles.cart_tab_item_counter_quantity}>{item.pivot.quantity}</span>
                                            <span onClick={() => incQun(item.pivot.id)}
                                                  className={styles.cart_tab_item_counter_decrement}>+</span>

                                        </div>
                                    </div>

                                </div>
                            ))}
                    </div>
                    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                </div>
                <Payment total={total}/>

            </>
        }

    </>

}