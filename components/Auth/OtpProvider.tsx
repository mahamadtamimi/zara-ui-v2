'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";
import Otp from "@/components/Auth/Otp";

export const OtpProvider = () => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <Otp />
        </PersistGate>
    </Provider>
}