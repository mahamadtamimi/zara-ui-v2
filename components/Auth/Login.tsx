'use client'
import Link from "next/link";

import styles from '@/style/auth.module.scss'
import {useFormState, useFormStatus} from "react-dom";
import {LoginForm} from "./action";
import {redirect, useSearchParams} from "next/navigation";
import Image from "next/image";
import loadingImage from "./load2.gif";
import {Input} from "@nextui-org/input";
import {EyeSlashFilledIcon} from "./EyeSlashFilledIcon";
import {EyeFilledIcon} from "./EyeFilledIcon";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button} from "@nextui-org/react";
import alertBox from "@/style/alertbox.module.scss";


const initialState = {
    message: null,
}

function SubmitButton() {

    const {pending} = useFormStatus()

    return (
        <button type="submit" className={styles.login_btn}>
            {pending ? <Image src={loadingImage} alt='' height={20} width={20}/> :
                'ورود'}
        </button>
    )
}


export default function Login() {

    const [isVisible, setIsVisible] = useState(false);
    const [pending, setPending] = useState(false);
    const [formData, setFormData] = useState({
        phoneNumber: {
            value: '',
            validate: false,
            massage: 'لطفا یک شماره تماس معتبر را وارد نمایید'
        },
        password: {
            value: '',
            validate: false
        }

    })
    const toggleVisibility = () => setIsVisible(!isVisible);


    const searchParams = useSearchParams()

    const back = searchParams.get('back')
    const [showAlertBox, setShowAlertBox] = useState({
        show: false,
        massage: ''
    })
    const dispatch = useDispatch()
    // @ts-ignore
    const user = useSelector((state) => state.user);

    const [formSuccess, setFormSuccess] = useState({success: false})

    function convert(string: any) {
        return string.replace(/[\u0660-\u0669\u06f0-\u06f9]/g, function (c: any) {
            return c.charCodeAt(0) & 0xf;
        });
    }

    function checkFormData(e: any) {
        switch (e.target.getAttribute('data-role')) {

            case 'phoneNumber':
                const phoneNum = convert(e.target.value)
                if (phoneNum.length > 11) return

                const phoneValidate = phoneNum.match(
                    /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/
                )

                setFormData({
                    ...formData,
                    phoneNumber: {
                        ...formData.phoneNumber,
                        value: phoneNum,
                        validate: phoneValidate,
                        massage: phoneValidate ? '' : 'لطفا یک شماره تماس معتبر را وارد نمایید',
                    }
                })


                break

            case 'password' :
                setFormData({
                    ...formData,
                    password: {
                        ...formData.password,
                        value: e.target.value,
                        validate: e.target.value.length > 0,

                    }
                })
        }
    }

    function submitHandle() {
        setPending(true)
        const fo = {
            'phone_number': formData.phoneNumber.value,
            'password': formData.password.value,
        };
        const requestUrl = `${process.env.API_PATH}/api/v1/login`
        const option = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(fo),
        }
        fetch(requestUrl, option).then(res => {
                console.log(res.status)
                setPending(false)
                if (res.status === 500) {
                    setShowAlertBox({
                        ...showAlertBox,
                        show: true,
                        massage: 'خطای سرور !  لطفا چند دقیقه دیگر امتحان کنید'
                    })

                    return

                }
                return res.json()
            }
        ).then(data => {
            console.log(data)
            if (!data.success) {
                setShowAlertBox({
                    ...showAlertBox,
                    show: true,
                    massage: data.error
                })

                return
            }

            if (data.success && data.user && data.token) {
                dispatch({type: 'LOGIN', payload: {user: data.user, token: data.token}});

                setFormSuccess({
                    success: data.success
                })
            }

            setShowAlertBox({
                ...showAlertBox,
                show: true,
                massage: 'خطای سرور !  لطفا چند دقیقه دیگر امتحان کنید'
            })

            return


        })
    }

    // if (state.success) {

    useEffect(() => {
        if (formSuccess.success) {
            switch (back) {
                case 'payment':
                    return redirect('/cart/get-address')


                default:
                    return redirect('/dashboard')
            }

        }
    }, [formSuccess]);

    console.log(back)
    const EmptySizeAlert = () => (
        <div className={alertBox.size_alert_box_sec}>
            <div className={alertBox.size_alert_box}>
                <div className={alertBox.size_alert_box_padding}>
                    <h3>
                        هشدار
                    </h3>
                    <p>{showAlertBox.massage}</p>

                </div>
                <button className={alertBox.size_alert_box_close} onClick={() => setShowAlertBox({
                    ...showAlertBox,
                    show: false,
                    massage: ''
                })}>
                    بستن
                </button>

            </div>
        </div>
    )
    return <>
        {showAlertBox.show && <EmptySizeAlert/>}
        <div className={styles.auth_sec}>

            <div className={styles.login_tab}>
                <span className={'title-typo'}>ورود به حساب کاربری</span>


                <div className={styles.login_tab_form}>
                    <div className={styles.login_tab_form_sec_div}>
                        <Input
                            data-role={'phoneNumber'}
                            variant={'underlined'}
                            classNames={{
                                label: 'input-label',
                                inputWrapper: 'input-wrapper'
                            }}
                            onChange={(e) => checkFormData(e)}
                            label={'شماره همراه'}
                            type="text"
                            name={'phoneNumber'}
                            value={formData.phoneNumber.value}
                            isInvalid={!formData.phoneNumber.validate}
                            errorMessage={!formData.phoneNumber.validate && formData.phoneNumber.massage}

                        />
                    </div>
                    <div className={styles.login_tab_form_sec_div}>
                        <Input
                            label="پسورد"
                            classNames={{
                                label: 'input-label',
                                inputWrapper: 'input-wrapper'
                            }}
                            onChange={(e) => checkFormData(e)}
                            data-role={'password'}
                            variant={'underlined'}
                            name={'password'}
                            endContent={
                                <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                    {isVisible ? (
                                        <EyeSlashFilledIcon className="text-2xl text-black pointer-events-none"/>
                                    ) : (
                                        <EyeFilledIcon className="text-2xl text-black pointer-events-none"/>
                                    )}
                                </button>
                            }
                            value={formData.password.value}
                            isInvalid={!formData.password.validate}
                            type={isVisible ? "text" : "password"}

                        />
                    </div>


                    <Button onClick={() => submitHandle()} radius={'none'}
                            isDisabled={!(formData.phoneNumber.validate && formData.password.validate)} type="submit"
                            className={styles.login_btn}>
                        {pending ? <Image src={loadingImage} alt='' height={20} width={20}/> :
                            'ورود'}
                    </Button>


                    <Link className={styles.forgot_pass_link} href={'/'}>رمز عبور خود را فراموش کرداید؟</Link>
                </div>

            </div>
            <div className={styles.register_tab}>
                <span className={'title-typo'}>
                    نیاز به ثبت نام دارید؟
                </span>

                <Button radius={'none'} as={Link} href={`/register${back ? `?back=${back}` : '' }`} className={styles.login_btn}>
                    ثبت نام
                </Button>

            </div>


        </div>

    </>
}