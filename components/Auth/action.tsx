'use server'

import signIn from "@/api/signIn";
import signUp from "@/api/signUp";


export async function LoginForm(prevState: any, formData: FormData) {


    const fo = {
        'phone_number': formData.get('phoneNumber'),
        'password': formData.get('password'),
    };


    console.log(fo)

    const data = await signIn(fo)

    // if (data.success === true) {
    // cookies().set({
    //     name: 'auth',
    //     value: data.token,
    //     httpOnly: true,
    //     path: '/',
    // })

    // cookies().set({
    //     name: 'user',
    //     value: JSON.stringify(data.user),
    //     httpOnly: true,
    //     path: '/',
    // })
    // }


    return data


}

export async function RegisterForm(prevState: any, formData: FormData) {


    const fo = {
        'email': formData.get('email'),
        'password': formData.get('password'),
        'name': formData.get('name'),
        'phone_number': formData.get('phoneNumber')
    };

    const data = await signUp(fo)

    // console.log(data)
    return data


}


export async function GetCodeForm(prevState: any, formData: FormData) {

    const requestUrl = `${process.env.API_PATH}/api/v1/auth/get-code`
    // 568073


    const res = await fetch(requestUrl)


    return res.json()


}

export async function ValidateCodeForm(prevState: any, formData: FormData) {

    const requestUrl = `${process.env.API_PATH}/api/v1/auth/validate-code`


    const data = {
        'code': formData.get('code'),
    };


    const res = await fetch(requestUrl)

    const datat = (res.json())
    console.log(datat)
    return datat

}