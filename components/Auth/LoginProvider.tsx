'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";
import Login from "./Login";

export const LoginProvider = () => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <Login/>
        </PersistGate>
    </Provider>
}