'use client'
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";

import Register from "@/components/Auth/Register";

export const RegisterProvider = () => {

    return <Provider store={store}>
        <PersistGate persistor={persistor}>
            <Register />
        </PersistGate>
    </Provider>
}