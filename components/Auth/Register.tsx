'use client'
import {useFormState, useFormStatus} from 'react-dom'
import Link from "next/link";
import styles from '@/style/auth.module.scss'
import alertBox from '@/style/alertbox.module.scss'
import {RegisterForm} from "./action";

import loadingImage from './load2.gif'
import Image from "next/image";
import React, {useEffect, useState} from "react";
import {Input} from "@nextui-org/input";

import {EyeSlashFilledIcon} from "@/components/Auth/EyeSlashFilledIcon";
import {EyeFilledIcon} from "@/components/Auth/EyeFilledIcon";
import {Button} from "@nextui-org/react";
import {useDispatch, useSelector} from "react-redux";
import {redirect, useSearchParams} from "next/navigation";

const initialState = {
    message: null,
}


export default function Register() {


    const dispatch = useDispatch()
    // @ts-ignore
    const user = useSelector((state) => state.user);

    const [success, setSuccess] = useState(false)


    const [showAlertBox, setShowAlertBox] = useState({
        show: false,
        massage: ''
    })


    const [isVisible, setIsVisible] = useState(false);
    const toggleVisibility = () => setIsVisible(!isVisible);

    const [formData, setFormData] = useState({
        name: {
            value: '',
            validate: false,
            massage: ''
        },
        email: {
            value: '',
            validate: '',
            massage: ''
        },
        password: {
            value: '',
            validate: '',
            massage: ''
        },
        rePassword: {
            value: '',
            validate: '',
            massage: ''
        },
        phoneNumber: {
            value: '',
            validate: '',
            massage: ''
        }
    })


    function updateFormData(e: any) {
        switch (e.target.getAttribute('data-role')) {
            case 'name' :

                const nameValidate = e.target.value.length > 2

                setFormData({
                    ...formData,
                    name: {
                        ...formData.name,
                        value: e.target.value,
                        validate: nameValidate,
                        massage: 'لطفا بیش از سه کاراکتر را وارد کنید'
                    }
                })
                break

            case 'email' :

                const email = e.target.value

                const validate = email.match(
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                )


                setFormData({
                    ...formData,
                    email: {
                        ...formData.email,
                        value: email,
                        validate: validate,
                        massage: validate ? '' : 'لطفا یک ایمیل معتبر را وارد نمایید',
                    }
                })


                break
            case 'phoneNumber' :
                const phoneNum = convert(e.target.value)
                if (phoneNum.length > 11) return
                const phoneValidate = phoneNum.match(
                    /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{5})$/
                )

                setFormData({
                    ...formData,
                    phoneNumber: {
                        ...formData.phoneNumber,
                        value: phoneNum,
                        validate: phoneValidate,
                        massage: phoneValidate ? '' : 'لطفا یک شماره تماس معتبر را وارد نمایید',
                    }
                })


                break

            case 'password' :
                const password = e.target.value
                const passwordValidate = password.length > 6

                setFormData({
                    ...formData,
                    password: {
                        ...formData.password,
                        value: password,
                        // @ts-ignore
                        validate: passwordValidate,
                        massage: 'لطفا بیش از شش کاراکتر را وارد نمایید'

                    }
                })

                break

            case 'rePassword' :
                const rePassword = e.target.value
                const rePasswordValidate = formData.password.value === rePassword


                setFormData({
                    ...formData,
                    rePassword: {
                        ...formData.rePassword,
                        value: rePassword,
                        // @ts-ignore
                        validate: rePasswordValidate,
                        massage: 'پسورد ها تطابق ندارند'

                    }
                })

                break

        }
    }


    function convert(string: any) {
        return string.replace(/[\u0660-\u0669\u06f0-\u06f9]/g, function (c: any) {
            return c.charCodeAt(0) & 0xf;
        });
    }

    const searchParams = useSearchParams()

    const [pending, setPending] = useState(false)
    const [formSuccess, setFormSuccess] = useState({success: false})
    const back = searchParams.get('back')
    useEffect(() => {

        if (formSuccess.success) {
            switch (back) {
                case 'payment':
                    return redirect(`/otp?back=${back}`)


                default:
                    return redirect('/otp')
            }

        }
        },
        [formSuccess]
    )

        const EmptySizeAlert = () => (
            <div className={alertBox.size_alert_box_sec}>
                <div className={alertBox.size_alert_box}>
                    <div className={alertBox.size_alert_box_padding}>
                        <h3>
                            هشدار
                        </h3>
                        <p>{showAlertBox.massage}</p>

                    </div>
                    <button className={alertBox.size_alert_box_close} onClick={() => setShowAlertBox({
                        ...showAlertBox,
                        show: false,
                        massage: ''
                    })}>
                        بستن
                    </button>

                </div>
            </div>
        )


        function handleSubmit() {
            setPending(true)
            const fo = {
                'email': formData.email.value,
                'password': formData.password.value,
                'name': formData.name.value,
                'phone_number': formData.phoneNumber.value
            };
            const requestUrl = `${process.env.API_PATH}/api/v1/register`
            const option = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(fo),
            }
            fetch(requestUrl, option).then(res => {
                    console.log(res.status)
                    setPending(false)
                    if (res.status === 500) {
                        setShowAlertBox({
                            ...showAlertBox,
                            show: true,
                            massage: 'خطای سرور !  لطفا چند دقیقه دیگر امتحان کنید'
                        })

                        return

                    }
                    return res.json()
                }
            ).then(data => {
                console.log(data)
                if (!data?.success) {
                    setShowAlertBox({
                        ...showAlertBox,
                        show: true,
                        massage: data.error
                    })

                    return
                }

                if (data.success && data.user && data.token) {
                    dispatch({type: 'LOGIN', payload: {user: data.user, token: data.token}});

                    setFormSuccess({
                        success: data.success
                    })
                } else {
                    setShowAlertBox({
                        ...showAlertBox,
                        show: true,
                        massage: 'خطای سرور !  لطفا چند دقیقه دیگر امتحان کنید'
                    })

                    return
                }


            })
        }

        function SubmitButton(props: any) {

            const {pending} = useFormStatus()


            // @ts-ignore
            return (

                <Button radius="none" disabled={!props.class} type="submit" onClick={() => handleSubmit()}
                        className={`${styles.login_btn}  ${!props.class && styles.disable}`}>
                    {pending ? <Image src={loadingImage} alt='' height={20} width={20}/> :
                        'ثبت نام'}
                </Button>
            )
        }

        return (
            <>
                {showAlertBox.show && <EmptySizeAlert/>}
                <div className={styles.register_sec_reg}>

                    <div className={styles.register_sec}>
                        <div className={styles.login_tab}>
                            <span className={'title-typo'}>ثبت نام</span>
                            <div className={styles.login_tab_form}>


                                <div className={styles.login_tab_form_sec_div}>
                                    <Input variant={'underlined'}
                                           classNames={{
                                               label: 'input-label',
                                               inputWrapper: 'input-wrapper'
                                           }}
                                           label={'نام'}
                                           data-role={'name'}
                                           onChange={(e) => updateFormData(e)}
                                           isInvalid={!formData.name.validate}
                                           errorMessage={!formData.name.validate && formData.name.massage}
                                           type="text"
                                           name={'name'}
                                           value={formData.name.value}
                                    />
                                </div>

                                <div className={styles.login_tab_form_sec_div}>
                                    <Input variant={'underlined'}
                                           classNames={{
                                               label: 'input-label',
                                               inputWrapper: 'input-wrapper'
                                           }}
                                           label={'ایمیل'}
                                           data-role={'email'}
                                           onChange={(e) => updateFormData(e)}
                                           isInvalid={!formData.email.validate}
                                           errorMessage={formData.email.massage}
                                           type="text"
                                           name={'email'}
                                           value={formData.email.value}

                                    />
                                </div>
                                <div className={styles.login_tab_form_sec_div}>
                                    <Input variant={'underlined'}
                                           classNames={{
                                               label: 'input-label',
                                               inputWrapper: 'input-wrapper'
                                           }}
                                           label={'شماره تماس'}
                                           data-role={'phoneNumber'}
                                           onChange={(e) => updateFormData(e)}
                                           isInvalid={!formData.phoneNumber.validate}
                                           errorMessage={formData.phoneNumber.massage}
                                           type="text"
                                           name={'phoneNumber'}
                                           value={formData.phoneNumber.value}

                                    />
                                </div>
                                <div className={styles.login_tab_form_sec_div}>
                                    <Input variant={'underlined'}
                                           classNames={{
                                               label: 'input-label',
                                               inputWrapper: 'input-wrapper'
                                           }}
                                           label={'پسورد'}
                                           data-role={'password'}
                                           onChange={(e) => updateFormData(e)}
                                           isInvalid={!formData.password.validate}
                                           errorMessage={!formData.password.validate && formData.password.massage}
                                           name={'password'}
                                           value={formData.password.value}
                                           endContent={
                                               <button className="focus:outline-none" type="button"
                                                       onClick={toggleVisibility}>
                                                   {isVisible ? (
                                                       <EyeSlashFilledIcon
                                                           className="text-xl  text-gray-600 pointer-events-none"/>
                                                   ) : (
                                                       <EyeFilledIcon
                                                           className="text-xl text-gray-600  pointer-events-none"/>
                                                   )}
                                               </button>
                                           }
                                           type={isVisible ? "text" : "password"}

                                    />
                                </div>
                                <div className={styles.login_tab_form_sec_div}>
                                    <Input variant={'underlined'}
                                           classNames={{
                                               label: 'input-label',
                                               inputWrapper: 'input-wrapper'
                                           }}
                                           label={'تکرار پسورد'}
                                           data-role={'rePassword'}
                                           onChange={(e) => updateFormData(e)}
                                           isInvalid={!formData.rePassword.validate}
                                           errorMessage={!formData.rePassword.validate && formData.rePassword.massage}
                                           name={'rePassword'}
                                           value={formData.rePassword.value}
                                           endContent={
                                               <button className="focus:outline-none" type="button"
                                                       onClick={toggleVisibility}>
                                                   {isVisible ? (
                                                       <EyeSlashFilledIcon
                                                           className="text-xl  text-gray-600 pointer-events-none"/>
                                                   ) : (
                                                       <EyeFilledIcon
                                                           className="text-xl text-gray-600  pointer-events-none"/>
                                                   )}
                                               </button>
                                           }
                                           type={isVisible ? "text" : "password"}
                                    />
                                </div>
                                <SubmitButton
                                    class={formData.name.validate && formData.phoneNumber.validate && formData.email.validate && formData.password.validate && formData.rePassword.validate}/>


                                <Link className={styles.forgot_pass_link} href={'/'}>
                                    رمز عبور خود را فراموش
                                    کرداید؟</Link>
                            </div>
                        </div>
                    </div>


                </div>
            </>

        )
    }