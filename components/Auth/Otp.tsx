'use client'

import styles from '@/style/auth.module.scss'

// import getOtp from "../../../api/OtpApi";
import {useFormState, useFormStatus} from "react-dom";
import Image from "next/image";
import loadingImage from "./load2.gif";
// import getCode from "../../../api/getCode";
import {GetCodeForm, ValidateCodeForm} from "./action";
import React, {useEffect, useState} from "react";

import {redirect, useSearchParams} from "next/navigation";
import {Input} from "@nextui-org/input";
import {Button, user} from '@nextui-org/react';
import {useDispatch, useSelector} from "react-redux";
import alertBox from "@/style/alertbox.module.scss";
import {set} from "immutable";


// import getOtp from "../../api/OtpApi";
const initialState = {
    message: null,
}

function SubmitButton(props: any) {

    const {pending} = useFormStatus()

    return (
        <Button radius={'none'} disabled={!props.class} type="submit"
                className={`${styles.login_btn}  ${!props.class && styles.disable}`}>
            {pending ? <Image src={loadingImage} alt='' height={20} width={20}/> :
                'تایید'}
        </Button>
    )
}


export default function Otp() {
    // getOtp()
    const dispatch = useDispatch()
    const [pending, setPending] = useState(false)
    const [useTimer, setUseTimer] = useState(false)
    const [state, codeFormAction] = useFormState(GetCodeForm, initialState)
    const [validateState, validateCodeFormAction] = useFormState(ValidateCodeForm, initialState)
    const [timer, setTimer] = useState(60)
    const [success, setSuccess] = useState(false)
    const [showAlert, setShowAlert] = useState({
        show: false,
        massage: ''
    })

    // @ts-ignore
    const user = useSelector((state) => state.user)
    // @ts-ignore
    const token = useSelector((state) => state.token)


    const [formData, setFormData] = useState({
        value: '',
        validate: false
    })

    useEffect(() => {
        if (timer === 0) {
            setUseTimer(false)
            setTimer(60)
        }

        if (useTimer) {
            const interval = setInterval(() => {
                setTimer(timer - 1)
            }, 1000);
            return () => clearInterval(interval);

        }


    }, [useTimer, timer, state]);

    function checkCode(e: any) {
        const code = convert(e.target.value)
        const codeValidate = code.length == 6
        if (code.length <= 6) {
            setFormData({
                ...formData,
                value: code,
                validate: codeValidate
            })

        }


    }

    function convert(string: any) {
        return string.replace(/[\u0660-\u0669\u06f0-\u06f9]/g, function (c: any) {
            return c.charCodeAt(0) & 0xf;
        });
    }


    function getCode() {

        setPending(true)

        const requestUrl = `${process.env.API_PATH}/api/v1/get-code`


        fetch(requestUrl, {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            redirect: "follow",
            referrerPolicy: "no-referrer",

        }).then(res => res.json()).then(data => {
            setPending(false)
            setUseTimer(true)
            console.log(data)
        })

    }


    function sendCode() {
        setPending(true)

        const requestUrl = `${process.env.API_PATH}/api/v1/validate-code`


        fetch(requestUrl, {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json",
                token: token
            },
            redirect: "follow",
            referrerPolicy: "no-referrer",
            body: JSON.stringify({code: formData.value})

        }).then(res => res.json()).then(data => {
            setPending(false)
            console.log(data)
            if (data.success) {
                dispatch({type: 'LOGIN', payload: {user: data.user, token: data.token}});
                setSuccess(true)
            } else {
                setShowAlert({...showAlert, show: true, massage: data.error})
            }


        })

    }
    const searchParams = useSearchParams()

    const back = searchParams.get('back')
    useEffect(() => {
        if (success) {
            switch (back) {
                case 'payment':
                    return redirect('/cart/get-address')


                default:
                    return redirect('/dashboard')
            }

        }

    }, [success])
    const GetCode = () => (
        <Button radius={'none'} onClick={() => getCode()} className={styles.login_btn}>
            {pending ? <Image src={loadingImage} alt='' height={20} width={20}/> :
                'ارسال کد'}
        </Button>
    )

    const EmptySizeAlert = () => (
        <div className={alertBox.size_alert_box_sec}>
            <div className={alertBox.size_alert_box}>
                <div className={alertBox.size_alert_box_padding}>
                    <h3>
                        هشدار
                    </h3>
                    <p>{showAlert.massage}</p>

                </div>
                <button className={alertBox.size_alert_box_close} onClick={
                    // @ts-ignore
                    () => setShowAlert(false)
                }>
                    بستن
                </button>

            </div>
        </div>
    )


    return <>
        {showAlert.show && <EmptySizeAlert/>}
        <div className={styles.otp_sec}>

            <div className={styles.auth_tab}>
                <form action={''}>
                    <div className={styles.auth_tab}>
                        <span className={'title-typo'}> کد ارسالی </span>
                        <div className={styles.auth_tab_form_sec_div}>

                            <Input variant={'underlined'}
                                   classNames={{
                                       label: 'input-label',
                                       inputWrapper: 'input-wrapper'
                                   }}
                                   label={'کد'}
                                   data-role={'name'}
                                   onChange={(e) => checkCode(e)}
                                   isInvalid={!formData.validate}
                                   errorMessage={!formData.validate && 'کد شش رقمی را وارد نمایید'}
                                   type="text"
                                   name={'code'}
                                   value={formData.value}/>


                            <span className={styles.alam_text}>
                                        {validateState.error}
                                </span>

                        </div>
                        <Button radius={'none'} disabled={!formData.validate} onClick={() => sendCode()}
                                className={`${styles.login_btn}  ${!formData.validate && styles.disable}`}>
                            {pending ? <Image src={loadingImage} alt='' height={20} width={20}/> :
                                'تایید'}
                        </Button>

                    </div>

                </form>

                {useTimer ? <p className={styles.sent_timer}>
                    ارسال مجدد : {timer} ثانیه
                </p> : <GetCode/>}


            </div>


        </div>
    </>
}