export async function getMainMenu() {
    const res = await fetch(`${process.env.API_PATH}/api/v1/get-menu` , { cache: 'no-store' })

    if (!res.ok) {

        throw new Error('Failed to fetch data')
    }
    return res.json()
}

