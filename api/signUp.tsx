
export default async function signUp(data : any) {

    const requestUrl = `${process.env.API_PATH}/api/v1/register`


    const res = await fetch(requestUrl , {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    })

    return res.json()
}