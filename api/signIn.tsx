

export default async function signIn(data : any) {

    const requestUrl = `${process.env.API_PATH}/api/v1/login`

    // const head:any = postHeader(data)

    const res = await fetch(requestUrl)



    if (!res.ok) {
        throw new Error('Failed to fetch data')
    }


    return res.json()
}