export async function getProducts(category: string, tag: string, type: string) {

    const res = await fetch(`${process.env.API_PATH}/api/v1/${category}/${tag}/${type !== undefined ? type : ''}`)
    //
    if (!res.ok) {

        throw new Error('Failed to fetch data')
    }
    return res.json()
}

