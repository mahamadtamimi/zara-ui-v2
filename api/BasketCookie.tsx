'use server'



export default async function AddBasket(data: any) {

    const requestUrl = `${process.env.API_PATH}/api/v1/add-to-basket`







    // console.log(data)
    const res = await fetch(requestUrl, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    })

    if (!res.ok) {
        throw new Error('Failed to fetch data')
    }

    const responseDate = await res.json()

    // cookies().set('name', 'lee')
    // cookies().set('basket_id' , response.id)
    // setCookieFunc(responseDate)
    return responseDate
}