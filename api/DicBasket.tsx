'use server'

import {cookies} from "next/headers";


export default async function DicBasket(data :any) {

    const requestUrl = `${process.env.API_PATH}/api/v1/dic-from-basket`


    // console.log(data)
    const res = await fetch(requestUrl, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data),
    })

    if (!res.ok) {
        throw new Error('Failed to fetch data')
    }

    const responseDate = await res.json()

    // cookies().set('name', 'lee')
    // cookies().set('basket_id' , response.id)
    // setCookieFunc(responseDate)
    return responseDate
}