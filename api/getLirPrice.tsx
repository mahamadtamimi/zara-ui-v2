export async function getLirPrice() {
    const res = await fetch(`${process.env.API_PATH}/api/v1/get-lir-price` , { cache: 'no-store' })

    if (!res.ok) {

        throw new Error('Failed to fetch data')
    }
    return res.json()
}

