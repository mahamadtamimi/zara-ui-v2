
import InnerPageTemplate from "@/template/InnerTemplate";
import {RegisterProvider} from "@/components/Auth/RegisterProvider";


export default async function Page({params}: { params: { slug: string } }) {
    return <InnerPageTemplate>
        <RegisterProvider/>
    </InnerPageTemplate>

}