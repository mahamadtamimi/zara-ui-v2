import React from "react";

import PaymentsGetAddress from "@/components/Cart/PaymentsGetAddress";
import InnerPageTemplate from "@/template/InnerTemplate";
import {PaymentsGetAddressProviders} from "@/components/Cart/PaymentsGetAddressProviders";


export default function page() {


    return <InnerPageTemplate>
        <PaymentsGetAddressProviders/>

    </InnerPageTemplate>
}