import InnerPageTemplate from "@/template/InnerTemplate";
import {CartReduxProvider} from "@/components/Cart/CartReduxProvider";

export default async function Page({params}: { params: { slug: string } }) {

    return <InnerPageTemplate>
        <CartReduxProvider/>
    </InnerPageTemplate>

}