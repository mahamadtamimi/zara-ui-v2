import Slider from '@/components/MainSlider/slider'
import HomeTemplate from "@/template/HomeTemplate";
import Head from "next/head";


export default async function Home() {
    const sliders =
        await fetch(`${process.env.API_PATH}/api/v1/get-slide`)

    const data = await  sliders.json()
// console.log(sliders.json())

    return (
        <HomeTemplate>

            <Slider data={data}/>
        </HomeTemplate>
    );
}
