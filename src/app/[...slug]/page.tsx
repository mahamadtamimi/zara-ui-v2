import {getProducts} from "@/api/getProducts";
import InnerPageTemplate from "@/template/InnerTemplate";
import InnerProductArchive from "@/components/ArchiveProduct/InnerProductArchive";
import {persistor, store} from "@/reduxStore/store";
import {PersistGate} from "redux-persist/integration/react";

import {Provider} from "react-redux";

export default async function Page({params}: { params: { slug: string } }) {

    const products = await getProducts(params.slug[0], params.slug[1], params.slug[2]);


    return <InnerPageTemplate>
        <InnerProductArchive products={products}
                             curent={[params.slug[0], params.slug[1], params.slug[2]]}/>

    </InnerPageTemplate>

}