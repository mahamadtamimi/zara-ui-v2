import InnerPageTemplate from "@/template/InnerTemplate";
import SearchPage from "@/components/SearchPage/SearchPage";

export default function page() {
    return <InnerPageTemplate>
        <SearchPage/>
    </InnerPageTemplate>


}