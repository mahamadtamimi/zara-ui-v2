import React from "react";
import {getProduct} from "@/api/getProduct";
import {getLirPrice} from "@/api/getLirPrice";
import InnerPageTemplate from "@/template/InnerTemplate";

import SingleProductReduxProvider from "@/components/SingleProduct/SingleProductReduxProvider";
import SingleTemplate from "@/template/SingleTemplate";


export default async function Page({params}: { params: { slug: string } }) {

    const product = await getProduct(params.slug, 1);
    const lirPrice = await getLirPrice()


    return <SingleTemplate>
        <SingleProductReduxProvider data={product} lir={lirPrice}/>
    </SingleTemplate>
}