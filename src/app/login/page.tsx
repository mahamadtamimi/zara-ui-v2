import {LoginProvider} from "@/components/Auth/LoginProvider";
import InnerPageTemplate from "@/template/InnerTemplate";


export default async function login({params}: { params: { slug: string } }) {


    return <InnerPageTemplate>
        <LoginProvider/>
    </InnerPageTemplate>

}