import '@/style/globals.css'
import type {Metadata} from 'next'
import localFont from 'next/font/local'
import React from "react";
import {Providers} from "./providers";


const myFont = localFont({
    src: './kalame/KalamehWeb-Light.woff2',
    display: 'swap',
    variable: '--iran-sens',
})

export const metadata: Metadata = {
    title: 'Zara Iran',
    description: '',
    icons: '/favicon.ico', // /public path
}

export default function RootLayout({
                                       children,
                                   }: {
    children: React.ReactNode
}) {
    return (
        <html lang="en" className={myFont.variable}>
        <body>
        <Providers>
            {children}
        </Providers>

        </body>
        </html>
    )
}
