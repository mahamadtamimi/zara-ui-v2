import InnerPageTemplate from "@/template/InnerTemplate";
import {AddNewAddressProvider} from "@/components/Dashboard/AddNewAddressProvider";

export default function page({ params }: { params: { id: number } }){



    return <InnerPageTemplate>
        <AddNewAddressProvider editAddress={params.id}/>
    </InnerPageTemplate>
}