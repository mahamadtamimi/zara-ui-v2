import React from "react";

import InnerPageTemplate from "@/template/InnerTemplate";
import AddNewAddress from "@/components/Dashboard/AddNewAddress";
import {AddressProvider} from "@/components/Dashboard/AddressProvider";
import {AddNewAddressProvider} from "@/components/Dashboard/AddNewAddressProvider";



export default async function Page({params}: { params: { slug: string } }) {

    // console.log(product)

    return <InnerPageTemplate>
        <AddNewAddressProvider/>
    </InnerPageTemplate>
}