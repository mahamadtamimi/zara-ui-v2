import React from "react";

import InnerPageTemplate from "@/template/InnerTemplate";
import {AddressProvider} from "@/components/Dashboard/AddressProvider";


export default async function Page({params}: { params: { slug: string } }) {
    return <InnerPageTemplate>
        <AddressProvider/>
    </InnerPageTemplate>
}