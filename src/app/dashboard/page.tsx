import InnerPageTemplate from "@/template/InnerTemplate";
import {DashboardProvider} from "@/components/Dashboard/DashboardProvider";

export default function page(){
    return <InnerPageTemplate>
        <DashboardProvider />
    </InnerPageTemplate>
}