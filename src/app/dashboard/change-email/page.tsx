import React from "react";
import InnerPageTemplate from "@/template/InnerTemplate";
import ChangeEmail from "@/components/Dashboard/ChangeEmail";
import { ChangeEmailProvider } from "@/components/Dashboard/ChangeEmailProvider";



export default async function Page({params}: { params: { slug: string } }) {
    return <InnerPageTemplate>
         <ChangeEmailProvider />
    </InnerPageTemplate>
}