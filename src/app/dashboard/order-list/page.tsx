import InnerPageTemplate from "@/template/InnerTemplate";
import {OrderListProvider} from "@/components/Dashboard/OderList/OrderListProvider";

export default function page() {
  return <InnerPageTemplate>
      <OrderListProvider />
  </InnerPageTemplate>

}