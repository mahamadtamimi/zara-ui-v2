import InnerPageTemplate from "@/template/InnerTemplate";
import OrderDetail from "@/components/Dashboard/OderList/OrderDetail";
import {OrderDetailProvider} from "@/components/Dashboard/OderList/OrderDetailProvider";

export default async function page({params}: { params: { slug: string } }) {
    // @ts-ignore
    const id = params.id


    return <InnerPageTemplate>
        <OrderDetailProvider orderId={id}/>
    </InnerPageTemplate>

}