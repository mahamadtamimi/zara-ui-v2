import React from "react";
import InnerPageTemplate from "@/template/InnerTemplate";
import {ChangePasswordProvider} from "@/components/Dashboard/ChangePasswordProvider";



export default async function Page({params}: { params: { slug: string } }) {
    return <InnerPageTemplate>
        <ChangePasswordProvider />
    </InnerPageTemplate>
}