import React from "react";
import InnerPageTemplate from "@/template/InnerTemplate";
import {ChangePhoneNumberProvider} from "@/components/Dashboard/ChangePhoneNumberProvider";



export default async function Page({params}: { params: { slug: string } }) {
    return <InnerPageTemplate>
        <ChangePhoneNumberProvider />
    </InnerPageTemplate>
}