import InnerPageTemplate from "@/template/InnerTemplate";
import {OtpProvider} from "@/components/Auth/OtpProvider";

export default function page() {
    return <InnerPageTemplate><OtpProvider/></InnerPageTemplate>
}