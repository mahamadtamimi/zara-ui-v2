
import React from "react";
import ThemeHeader from "@/components/Globals/mainMenu/ThemeHeader";
import Head from "next/head";

export default function HomeTemplate({ children }: { children: React.ReactNode }) {
    return <>
        <ThemeHeader indexPage={true} singlePage={false}/>

        {children}
    </>

}