import React from "react";
import ThemeHeader from "@/components/Globals/mainMenu/ThemeHeader";


export default function SingleTemplate({children}: { children: React.ReactNode }) {
    return <>
        <ThemeHeader indexPage={false} singlePage={true} />
        {children}
    </>



}