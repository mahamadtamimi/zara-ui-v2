import {legacy_createStore as createStore, Store, applyMiddleware} from 'redux';
import {reducer} from './features/userReducer';
import {persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import persistStore from 'redux-persist/es/persistStore';

const persistConfig = {
    key: 'root',
    storage: storage,
}

const persistedReducer = persistReducer(persistConfig, reducer);
const store: Store = createStore(
    persistedReducer,
);

const persistor = persistStore(store);
export {store, persistor};