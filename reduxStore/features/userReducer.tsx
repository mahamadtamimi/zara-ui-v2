import {initialState} from "./initialState"
// @ts-ignore
import {State} from "./initialState"

export function reducer(state: State = initialState, action: any) {
    switch (action.type) {
        case "LOADING":
            return {
                ...state,
                loading: action.payload.loading,

            }


        case "LOGIN":
            return {
                ...state,
                user: action.payload.user,
                token: action.payload.token,

            }


        case "UPDATE_BASKET_ID":
            return {
                ...state,
                basket_id: action.payload
            }
        case "UPDATE_BASKET_PRODUCT":
            return {
                ...state,
                basket_product: action.payload
            }


        case "USER_SIZE_UPDATE":
            return {
                ...state,
                size: {
                    weight: action.payload.weight,
                    height: action.payload.height,
                    style: action.payload.style,

                }
            }

        default:
            return state;
    }
}