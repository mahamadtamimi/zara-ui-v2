export const initialState = {
    user: null,
    token: null,
    basket_id: null,
    basket_product: {},
    size: {
        weight : 0,
        height : 0 ,
        style : 0
    },

    loading: true

}